#! /usr/bin/python3.6

from time import sleep, time, strftime
from argparse import ArgumentParser
from io import BytesIO
# from pathlib import Path
import subprocess
import sys
import os
import time

import numpy as np      # pyre-ignore[21]
# Undefined import [21]: Could not find a module corresponding to import

from picamera import PiCamera   # pyre-ignore[21]
# Undefined import [21]: Could not find a module corresponding to import

from atollogy.raspberry.image_collector.auto_exposure.ae_detection import PiCameraHelper
from atollogy.raspberry.image_collector.auto_exposure.ae_logger import AELoggerStyle
from atollogy.raspberry.image_collector.auto_exposure.auto_exposure_setting_reader import RegionDetails, \
    AutoExposureSettingReader
from atollogy.raspberry.image_collector.auto_exposure.file_auto_exposure_setting_processor import PiCameraSetting, \
    FileAutoExposureSettingProcessor


def main():
    parser = ArgumentParser(description='Finds the proper exposure and takes a picture')

    '''Program Initialization Arguments'''
    parser.add_argument('--iso', choices=[0, 100, 200, 400, 800], type=int, dest='iso')
    parser.add_argument('--resolution', nargs=2, type=int, dest='resolution')
    parser.add_argument('--shutterspeed', '-s', type=int, dest='starting_shutter_speed')
    parser.add_argument('--whitebalance', nargs=2, type=float, dest='wb_gains')
    parser.add_argument('--maxframerate', '-f', type=float, dest='max_framerate')
    parser.add_argument('--vflip', '-v', action='store_true', dest='vflip')
    parser.add_argument('--timeout', '-t', type=int, dest='timeout')

    '''Auto Exposure Loop Parameters'''
    parser.add_argument('--regionname', type=str, dest='region_name')
    parser.add_argument('--region', nargs=4, type=int, dest='region')
    parser.add_argument('--optbrightness', type=int, dest='optimal_brightness_value')
    parser.add_argument('--threshold', type=float, dest='cuttoff_threshold')
    parser.add_argument('--maxexposuretime', type=int, dest='max_exposure_time')
    parser.add_argument('--rotation', type=int, dest='rotation')

    parser.add_argument('--hdr', action='store_true', dest='hdr')

    args = parser.parse_args()

    '''Try to load information from the configuration file'''

    '''Turn Namespace object into dictionary, then remove all none values'''
    args = vars(args)
    args = {k:v for k, v in args.items() if v is not None}

    image_bytes = expose_and_capture(**args)

    '''sys.stdout doesn't work with bytes objects
        fd=1 for writing to standard output'''
    image_bytes.seek(0)
    os.write(1, image_bytes.read())


def _debug_log(msg_str):
    sys.stderr.write("ae_camera.py: ppid={} pid={} {}\n".format(os.getppid(),os.getpid(),msg_str))

_OP_ACQUIRE_CAM_OBJ = "acquire_cam_obj"
_OP_CAPTURE_PHOTO_HDR = "capture_photo_hdr"
_OP_ENFUSE_PROCESS = "enfuse"

# assuming this python is run in a single threaded fashion
# For this dictionary, the key is Operation Name and value is time in Epoch millisecond
_OP_TIME_DICT = {}

def _debug_timed_op_start(op_name_str):
    _debug_log(op_name_str + " started")
    _OP_TIME_DICT[op_name_str] = int(round(time.time()*1000))


def _debug_timed_op_end(op_name_str):
    start_time = _OP_TIME_DICT.get(op_name_str)
    if start_time:
        cur_time = int(round(time.time()*1000))
        time_elapsed = cur_time - start_time
        _debug_log(op_name_str + " ended ; time_elapsed={}".format(time_elapsed))
    else:
        _debug_log(op_name_str + " ended")


def expose_and_capture(resolution=(480, 240), iso=800, starting_shutter_speed=5000,
                       max_framerate=20, vflip=False, rotation=0, timeout=30, wb_gains=None,
                       region=None, hdr=False, region_name="default_region_name", **kwargs):
    '''Setup pi camera and capture single image

        Keyword arguments:
        resolution: list            -- [width: int, height: int] (1664, 1232)
        iso: int                    -- value in {0, 100, 200, 400, 800}
        starting_shutter_speed: int -- shutter speed camera is initialized to
        max_framerate: int          -- framerate cap
        vflip: boolean              -- vertical flip
        timeout: int                -- maximum time to run the exposure loop
        wb_gains: list              -- (red_gains, blue_gains)
        region                      -- (xmin, xmax, ymin, ymax)
        kwargs                      -- passed to auto_exposure_loop

        returns BytesIO jpeg encoded image'''

    # For max resolution [ e.g. (3280, 2464) ],
    # the frame rate cannot be as low as 10. It needs to be 20,
    # in order to avoid "PiCameraMMALError":
    #
    # picamera.exc.PiCameraMMALError: Failed to enable component:
    #  Out of resources (other than memory)
    #

    setting_cache_dir = "/var/lib/atollogy/ae_camera/setting_cache"
    os.makedirs(setting_cache_dir, exist_ok = True)

    _debug_timed_op_start(_OP_ACQUIRE_CAM_OBJ)
    with PiCamera() as camera:
        _debug_timed_op_end(_OP_ACQUIRE_CAM_OBJ)

        pi_camera_setting = PiCameraSetting( resolution = resolution, iso = iso,
                 starting_shutter_speed = starting_shutter_speed,
                 max_framerate = max_framerate,
                 vflip = vflip,
                 rotation = rotation,
                 ae_loop_timeout = timeout,
                 wb_gains = wb_gains)

        pi_camera_helper = PiCameraHelper(camera, AELoggerStyle.STD_ERR)

        # pi_camera_helper.detect_ae_shutter_speed(pi_camera_setting, region_obj)
        auto_exposure_setting_reader = FileAutoExposureSettingProcessor(
            setting_directory_abs_path=setting_cache_dir,
            pi_camera_setting=pi_camera_setting,
            fs_caching_ttl_in_second=5*60,     # 5 minutes
            memory_caching_ttl_in_second=60,   # 1 minute
            pi_camera_helper=pi_camera_helper,
            ae_logger_style=AELoggerStyle.STD_ERR)

        optimal_brightness_value = kwargs.get("optimal_brightness_value")

        if region and region_name:
            region_obj = RegionDetails(region_name=region_name,
                                       min_x=region[0], min_y=region[2],
                                       width=region[1]-region[0],
                                       height=region[3]-region[2],
                                       optimal_brightness_value=optimal_brightness_value)
            setting = auto_exposure_setting_reader.read_region_auto_exposure_setting(region_obj)
        else:
            setting = auto_exposure_setting_reader.read_default_region_auto_exposure_setting()

        pi_camera_helper.init_camera_setting(pi_camera_setting)
        camera.shutter_speed = setting.shutter_speed
        sys.stderr.write(
            'ae_camera.py: shutter_speed={} \n'.format(setting.shutter_speed))
        sleep(0.1)
        # not sure whether we need to sleep after shutter speed is changed

        if not hdr:
            image = BytesIO()
            camera.capture(image, 'jpeg')
            sys.stderr.write('ae_camera.py: Returning auto_exposed image\n')
            return image
        else:
            sys.stderr.write('ae_camera.py: Taking HDR shot\n')
            return hdrShot(camera, camera.exposure_speed)


def hdrShot(camera, current_ss: int):

    PERCENT_OF = .75

    delta_ss= int(current_ss*PERCENT_OF)
    bracket = [current_ss - delta_ss, current_ss, current_ss + delta_ss]
    images = []

    for shutterspeed in bracket:

        iname = '/tmp/ss-%d.jpg' % (shutterspeed)
        images.append(iname)
        _debug_log("hdr: shutter_speed={}".format(shutterspeed))
        camera.shutter_speed = shutterspeed
        _debug_timed_op_start(_OP_CAPTURE_PHOTO_HDR)
        camera.capture(iname)
        _debug_timed_op_end(_OP_CAPTURE_PHOTO_HDR)

    f = open('/tmp/hdrstack', 'w')
    for file in images:
        f.write(file +'\n')
    f.close()


    year, month, day, hour, minute = strftime("%Y,%m,%d,%H,%M").split(',')

    image_name = 'ExposueFusion:%s_%s.jpg' % (hour, minute)

    outfile = '--output=%s' % ('/tmp/' + image_name)

    # stack = []

    FNULL = open(os.devnull, 'w')
    _debug_timed_op_start(_OP_ENFUSE_PROCESS)
    subprocess.call(["enfuse", outfile, "@/tmp/hdrstack"], stdout=FNULL, stderr=subprocess.STDOUT)
    _debug_timed_op_end(_OP_ENFUSE_PROCESS)
    script_path = '/tmp'
    image_stack = script_path + '/hdrstack'
    fusion_path = script_path + '/' + image_name

    #for exposure in images:
    #    os.remove(exposure)

    os.remove(image_stack)
    image_object = open(fusion_path, 'rb')
    # date = int(time())

    sys.stderr.write('ae_camera.py: Returning HDR image\n')
    return image_object


if __name__ == '__main__':
    main()
