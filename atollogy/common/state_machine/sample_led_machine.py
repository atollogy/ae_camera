import time
import asyncio
from enum import Enum

from atollogy.common.state_machine.single_state_machine import \
        ActionContext, SingleStateMachine, TransitionDef, StateDef

try:
    import blinkt       # pylint: disable=import-error  # pyre-ignore[21]
    # Unable to import 'blinkt' (import-error)
    BLINKT_EXISTS = True
except ModuleNotFoundError:
    BLINKT_EXISTS = False

def _test_clear_cur_line():
    print(chr(27) + "[2K\r", end='', flush=True)

def _test_print_without_newline(input_str):
    print(input_str, end='', flush=True)

def _test_cur_time_in_milli():
    return int(round(time.time() * 1000))

class Color(Enum):
    YELLOW = (255, 128, 0)
    GREEN = (0, 255, 0)
    BLUE = (0, 0, 255)
    NONE = (0, 0, 0)

_COLOR_TRANSLATION = {
    Color.YELLOW: '[ (Y) ( ) ]',
    Color.GREEN:  '[ ( ) (G) ]',
    Color.BLUE:   '[ (B) ( ) ]',
    Color.NONE:   '[ ( ) ( ) ]',
}

def _get_color(var):
    if isinstance(var, Color):
        return var

    if isinstance(var, str):
        color = Color[var.upper()]
        return color
    return Color.NONE

def _set_first(color):
    true_color = _get_color(color)
    if BLINKT_EXISTS:
        blinkt.set_pixel(0, *true_color.value)
        blinkt.show()
    else:
        _test_clear_cur_line()
        _test_print_without_newline(_COLOR_TRANSLATION[true_color])

async def _test_solid_b(action_context: ActionContext) -> None:
    _set_first("blue")
    while not action_context.state_machine_has_been_shutdown() \
            and not action_context.get_new_state():
        # print("_tmp: solid_y ...")
        # t1 = _test_cur_time_in_milli()
        await action_context.wait_for_new_state(1.0)
        # t2 = _test_cur_time_in_milli()
        # print("_tmp: solid_y elapsed={} ".format((t2-t1)))
    # print("_tmp: _test_solid_y: new_state = {}".format(action_context.get_new_state()))
    action_context.mark_action_completed()

async def _test_solid_g_for_n_seconds(action_context: ActionContext) -> None:
    _set_first("green")
    await asyncio.sleep(3)   # solid green for 3 seconds
    action_context.mark_action_completed()

async def _test_blink_y_g(action_context: ActionContext) -> None:
    while not action_context.state_machine_has_been_shutdown() \
            and not action_context.get_new_state():
        _set_first("yellow")
        await asyncio.sleep(0.5)
        if action_context.get_new_state():
            action_context.mark_action_completed()
            return
        _set_first("green")
        await asyncio.sleep(0.5)
        if action_context.get_new_state():
            action_context.mark_action_completed()
            return
    action_context.mark_action_completed()

async def _test_blink_y(action_context: ActionContext) -> None:
    while not action_context.state_machine_has_been_shutdown() \
            and not action_context.get_new_state():
        _set_first("yellow")
        await asyncio.sleep(0.5)
        if action_context.get_new_state():
            action_context.mark_action_completed()
            return
        _set_first("none")
        await asyncio.sleep(0.5)
        if action_context.get_new_state():
            action_context.mark_action_completed()
            return
    action_context.mark_action_completed()

async def _test_blink_g(action_context: ActionContext) -> None:
    while not action_context.state_machine_has_been_shutdown() \
            and not action_context.get_new_state():
        _set_first('green')
        await asyncio.sleep(0.5)
        if action_context.get_new_state():
            action_context.mark_action_completed()
            return
        _set_first("none")
        await asyncio.sleep(0.5)
        if action_context.get_new_state():
            action_context.mark_action_completed()
            return
    action_context.mark_action_completed()

def _test_init_state_machine() -> SingleStateMachine:
    ready_state_def = StateDef("ready")
    ready_state_def.action = _test_solid_b
    ready_state_def.transitions["wait_on_ae"] = TransitionDef()
    ready_state_def.transitions["wait_on_motion"] = TransitionDef()

    async def _dummy_transition_action(action_context: ActionContext) -> None:
        # pylint: disable=unused-argument
        return None

    ready_state_def.transitions["wait_on_ae"].transiting_action = _dummy_transition_action

    w2_state_def = StateDef("wait_on_ae")
    w2_state_def.action = _test_blink_y
    w2_state_def.transitions["wait_on_motion"] = TransitionDef()
    w2_state_def.transitions["ready"] = TransitionDef()

    w3_state_def = StateDef("wait_on_motion")
    w3_state_def.action = _test_blink_y_g
    w3_state_def.transitions["notify_complete"] = TransitionDef()

    notify_state_def = StateDef("notify_complete")
    notify_state_def.action = _test_solid_g_for_n_seconds
    notify_state_def.transitions["ready"] = TransitionDef()

    state_def_list = [ready_state_def, w2_state_def, w3_state_def, notify_state_def]
    machine = SingleStateMachine("ready", state_def_list)
    return machine


def _sync_test():
    machine = _test_init_state_machine()
    try:
        machine.start()
        time.sleep(2)
        machine.transit_to_new_state("wait_on_ae")
        time.sleep(5)
        machine.transit_to_new_state("ready")
        time.sleep(5)
        machine.transit_to_new_state("wait_on_ae")
        time.sleep(5)
        machine.transit_to_new_state("wait_on_motion")
        time.sleep(10)
        machine.transit_to_new_state("notify_complete")
        time.sleep(0.5)
        machine.transit_to_new_state("ready")
        time.sleep(8)
    except ValueError as value_err:
        print()
        print(value_err)
    finally:
        print()
        machine.shutdown()

_sync_test()
