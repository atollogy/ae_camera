
# pyre-strict

import asyncio
import os
from concurrent.futures import ThreadPoolExecutor
import logging
from logging.handlers import RotatingFileHandler
from typing import Callable, List, Awaitable, Dict, Optional


class ActionContext:

    def __init__(self, cur_state: str, transition_action: bool = False) -> None:
        self._cur_state: str = cur_state
        self._transition_action: bool = transition_action

        self._state_machine_has_been_shutdown: bool = False
        self._new_state: Optional[str] = None
        self._new_state_event: asyncio.Event = asyncio.Event()

        self._action_completed: bool = False
        self._action_completed_event: asyncio.Event = asyncio.Event()

    def state_machine_to_be_shutdown(self) -> None:
        self._state_machine_has_been_shutdown = True

    def state_machine_has_been_shutdown(self) -> bool:
        return self._state_machine_has_been_shutdown

    def set_action_completed(self) -> None:
        self._action_completed = True

    def get_action_completed(self) -> bool:
        return self._action_completed

    def get_cur_state(self) -> str:
        return self._cur_state

    def set_new_state(self, new_state: str) -> None:
        self._new_state = new_state
        self._new_state_event.set()

    def get_new_state(self) -> Optional[str]:
        return self._new_state

    def is_transition_action(self) -> bool:
        return self._transition_action

    async def wait_for_new_state(self, timeout: float) -> Optional[str]:
        try:
            await asyncio.wait_for(self._new_state_event.wait(), timeout=timeout)
        except asyncio.TimeoutError:
            pass
        return self._new_state

    def mark_action_completed(self) -> None:
        self._action_completed_event.set()

    async def wait_for_action_completed(self, timeout: float) -> bool:
        try:
            await asyncio.wait_for(self._action_completed_event.wait(), timeout=timeout)
        except asyncio.TimeoutError:
            pass
        return bool(self._action_completed_event.is_set())


class TransitionDef:
    '''
    This should be an Immutable object.
    '''
    transiting_action: Optional[Callable[[ActionContext], Awaitable[None]]] = None

class StateDef:
    '''
    This should be an Immutable object.
    '''
    def __init__(self, name: str) -> None:
        self.name: str = name
        self.desc: Optional[str] = None
        self.action: Optional[Callable[[ActionContext], Awaitable[None]]] = None
        self.transitions: Dict[str, TransitionDef] = {}
        # This "transitions" dictionary has state name str as Key, TransitionDef as Value.

class SingleStateMachine:

    def __init__(self, start_state: str, state_def_list: List[StateDef]) -> None:

        self._state_def_dict: Dict[str, StateDef] = {}
        for state_def in state_def_list:
            self._state_def_dict[state_def.name] = state_def

        self._state_machine_has_been_shutdown: bool = False

        start_state_def = self._state_def_dict.get(start_state)
        if not start_state_def:
            raise ValueError("start_state '{}' is not defined.".format(start_state))
        self._cur_state: str = start_state


        self._last_action_context: Optional[ActionContext] = None
        self._action_completion_deadline_duration: float = 10.0
        # '_action_completion_deadline_duration' means the duration (in number of seconds)
        # that this state machine allows an State Action to complete,
        # after the caller of this State Machine requests to transit a new state.
        # Currently, it is set to 10 seconds

        self._executor: Optional[ThreadPoolExecutor] = None
        self._asyncio_event_loop: Optional[asyncio.AbstractEventLoop] = None
        self._init_logger()

    def start(self) -> None:
        self._logger.info("start() invoked")

        self._asyncio_event_loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self._asyncio_event_loop)

        self._executor = ThreadPoolExecutor(max_workers=1)
        self._executor.submit(self._thread_start)

    def get_cur_state(self) -> str:
        return self._cur_state

    def transit_to_new_state(self, new_state: str) -> None:

        if self._state_machine_has_been_shutdown:
            raise RuntimeError(
                'Cannot transit to a new state, '
                'as this state machine has been shutdown.')

        self._logger.info("transit_to_new_state(): invoked: state=%s new_state=%s",
                          self._cur_state, new_state)

        # check whether there is a defined transition from _cur_state to new_state
        state_def = self._state_def_dict.get(self._cur_state)
        if state_def:
            new_state_transition_def = state_def.transitions.get(new_state)

            if new_state_transition_def:
                new_state_def = self._state_def_dict.get(new_state)
                if new_state_def:
                    if self._last_action_context and self._executor:
                        if self._last_action_context.get_new_state():
                            raise ValueError(
                                "cannot request transit to a new state more than once. ")

                        self._helper_set_new_state(new_state)
                else:
                    raise ValueError(
                        "state definition of new state cannot be found: new_state={}".format(
                            new_state))
            else:
                raise ValueError(
                    "transition cannot be found: cur_state={} new_state={}".format(
                        self._cur_state, new_state))
        else:
            raise ValueError(
                "state definition of current state cannot be found: cur_state={}".format(
                    self._cur_state))

    def shutdown(self) -> None:
        self._logger.info("shutdown() invoked")
        self._state_machine_has_been_shutdown = True

        if self._last_action_context:
            self._last_action_context.state_machine_to_be_shutdown()
        if self._executor:
            self._executor.shutdown()

    def _helper_set_new_state(self, new_state: str) -> None:
        if self._asyncio_event_loop and self._last_action_context and self._executor:
            self._asyncio_event_loop.call_soon_threadsafe(
                self._last_action_context.set_new_state, new_state)
            # this 'set_new_state' must be called within the context of
            # the asyncio event loop.
            # Otherwise, the _new_state_event won't be notified properly.
            self._executor.submit(self._thread_actions_for_new_state)

    def _init_logger(self, log_level: int = logging.DEBUG) -> None:
        log_formatter = logging.Formatter(
            '%(asctime)s %(levelname)s name=%(name)s %(message)s')

        log_file_dir = "/var/lib/atollogy/state_machine/logs/"
        # hardcoded log file location for now

        os.makedirs(log_file_dir, exist_ok=True)
        log_file_loc = log_file_dir + "single_state_machine.log"

        file_handler = RotatingFileHandler(log_file_loc, mode='a',
                                           encoding=None, delay=False,
                                           maxBytes=5 * 1024 * 1024,  # 5 MB
                                           backupCount=2)
        # we may switch to other file handlers in future,

        file_handler.setFormatter(log_formatter)
        file_handler.setLevel(log_level)

        self._logger: logging.Logger = logging.getLogger(__name__)
        self._logger.setLevel(log_level)
        # hardcoded log level for now

        self._logger.addHandler(file_handler)

    def _thread_start(self) -> None:
        if self._asyncio_event_loop:
            self._asyncio_event_loop.run_until_complete(self._async_start())

    async def _async_start(self) -> None:
        action_context = ActionContext(self._cur_state)
        self._last_action_context = action_context
        try:
            self._logger.info("_async_start(): start state action: about to be invoked: state=%s",
                              self._cur_state)
            await self._state_def_dict[self._cur_state].action(         # pyre-ignore[29]
                action_context)
            # `Optional[typing.Callable[[ActionContext], Awaitable[None]]]` is not a function.
            self._logger.info("_async_start(): start state action: completed: state=%s",
                              self._cur_state)
        except Exception as err:       # pylint: disable=broad-except
            self._logger.error(
                "_async_start(): start state action: error: state=%s type(err)=%s err=%s",
                self._cur_state, type(err), err)

    def _thread_actions_for_new_state(self) -> None:
        if self._asyncio_event_loop:
            self._asyncio_event_loop.run_until_complete(self._async_actions_for_new_state())

    async def _async_actions_for_new_state(self) -> None:
        state_def = self._state_def_dict.get(self._cur_state)
        if state_def:
            if self._last_action_context:
                new_state = self._last_action_context.get_new_state()
                if new_state:
                    self._logger.info(
                        '_async_actions_for_new_state(): triggered: '
                        'wait for last action completed: state=%s new_state=%s',
                        self._cur_state, new_state)
                    action_completed = await self._last_action_context.wait_for_action_completed(
                        timeout=self._action_completion_deadline_duration)
                    self._logger.info(
                        '_async_actions_for_new_state(): triggered: '
                        'last action completed: state=%s new_state=%s',
                        self._cur_state, new_state)

                    if not action_completed:
                        msg = ("_async_actions_for_new_state(): error: "
                               +"Action for cur_state={} cannot be completed within {} seconds "
                               +"after being triggered to new state").format(
                                   self._cur_state, self._action_completion_deadline_duration)
                        self._logger.error(msg=msg)
                        raise RuntimeError(msg)

                    if self._state_machine_has_been_shutdown:
                        raise RuntimeError(
                            'Cannot transit to a new state, '
                            'as this state machine has been shutdown.')

                    new_state_transition_def = state_def.transitions.get(new_state)
                    if new_state_transition_def and new_state_transition_def.transiting_action:
                        action_context_01 = ActionContext(self._cur_state, transition_action=True)
                        action_context_01.set_new_state(new_state)
                        self._last_action_context = action_context_01
                        self._logger.info(
                            '_async_actions_for_new_state(): '
                            'transition action: about to be invoked: state=%s new_state=%s',
                            self._cur_state, new_state)
                        try:
                            await new_state_transition_def.transiting_action(action_context_01)
                            self._logger.info(
                                '_async_actions_for_new_state(): '
                                'transition action: completed: state=%s new_state=%s"]',
                                self._cur_state, new_state)
                        except Exception as err:  # pylint: disable=broad-except
                            self._logger.error(
                                '_async_actions_for_new_state(): '
                                'transition action: error: '
                                'state=%s new_state=%s type(err)=%s err=%s',
                                self._cur_state, new_state, type(err), err)


                    action_context_02 = ActionContext(new_state)
                    old_state = self._cur_state
                    self._cur_state = new_state
                    self._last_action_context = action_context_02
                    self._logger.info('_async_actions_for_new_state(): '
                                      'state changed: state=%s new_state=%s',
                                      old_state, new_state)
                    self._logger.info(
                        '_async_actions_for_new_state(): '
                        'state action: about to be invoked: state=%s',
                        self._cur_state)
                    try:
                        await self._state_def_dict[self._cur_state].action(     # pyre-ignore[29]
                            action_context_02)
                        # `Optional[typing.Callable[[ActionContext], Awaitable[None]]]``
                        # is not a function.
                        self._logger.info(
                            "_async_actions_for_new_state(): state action: completed: state=%s",
                            self._cur_state)
                    except Exception as err:  # pylint: disable=broad-except
                        self._logger.error(
                            '_async_actions_for_new_state(): '
                            'state action: error: state=%s type(err)=%s err=%s',
                            self._cur_state, type(err), err)
