
# pyre-strict

from abc import ABC, abstractmethod
from concurrent.futures import ThreadPoolExecutor
from threading import Thread
from typing import Callable, Dict, Iterator, List

class MonitorLoopCallbackMgr(ABC):
    '''
    This class uses "threading" and "ThreadPoolExecutor".
    Its accepts a list of Callable for Callback handler.
    This is an abstract class, which contains an abstract method "monitor_callee_iterator",
    which has no input arguments and return an Iterator[Dict].
    The Dict value from this Iterator will be used in the Callback input arguments.


    Regarding to choice to Concurrency / Parallelism, please see also:
    http://masnun.rocks/2016/10/06/async-python-the-different-forms-of-concurrency/

    '''
    def __init__(self, callback_list: List[Callable],
                 thread_pool_size: int) -> None:
        self._callback_list = callback_list
        self._callback_thread_pool = ThreadPoolExecutor(    # pyre-ignore[4]
            max_workers=thread_pool_size)
        self._event_monitor_thread = Thread(target=self._monitor_caller_loop,   # pyre-ignore[4]
                                            daemon=True)
        self._state = 1     # pyre-ignore[4]
        # 1 = initiated
        # 2 = started
        # 3 = stopped / already shutdown

        # the pyre ignore [4] is added only because of running on Python 3.5 on Raspberry Pi
        # Python 3.5 does not support object attribute annotation syntax yet. E.g.:
        # self._event_monitor_thread: Thread = Thread(...)


    def start(self) -> None:
        self._state = 2
        self._event_monitor_thread.start()

    def shutdown(self) -> None:
        self._callback_thread_pool.shutdown()
        del self._callback_thread_pool
        del self._event_monitor_thread
        self._state = 3

    def get_state(self) -> int:
        return self._state

    @abstractmethod
    def monitor_callee_iterator(self) -> Iterator[Dict]:
        raise NotImplementedError

    def __enter__(self) -> None:
        # used in "with" statement
        # see also: https://www.python.org/dev/peps/pep-0343/
        self.start()

    def __exit__(self, exception_type, exception_value, traceback) -> None:     # pyre-ignore[2]
        # Missing parameter annotation [2]: Parameter `exception_type` has no type specified.
        # used in "with" statement
        # see also: https://www.python.org/dev/peps/pep-0343/
        self.shutdown()

    def _monitor_caller_loop(self) -> None:
        for result in self.monitor_callee_iterator():
            for callback in self._callback_list:
                # callback(**result)
                self._callback_thread_pool.submit(callback, **result)
            if self._state == 3:
                print("monitor caller loop is stopped")
                return
