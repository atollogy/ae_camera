# pyre-strict

import datetime

import os
import os.path
import time
from typing import Optional, Tuple
import functools
import numpy as np # pyre-ignore[21]

# Undefined import [21]: Could not find a module corresponding to import

from atollogy.raspberry.image_collector.auto_exposure.ae_detection \
    import PiCameraHelper, PiCameraSetting
from atollogy.raspberry.image_collector.auto_exposure.ae_logger \
    import AELogger, AELoggerStyle
from atollogy.raspberry.image_collector.auto_exposure.auto_exposure_setting_reader \
    import RegionDetails, AutoExposureSettingReader
from atollogy.raspberry.image_collector.auto_exposure.file_auto_exposure_setting_processor \
    import FileAutoExposureSettingProcessor
from atollogy.raspberry.image_collector.motion_detection.motion_state_machine \
    import MotionStateMachine, STATE_AE

TAKE_PICTURE: str = "Take Picture"
GRAYSCALE_CONVERSTION: str = "Grayscale-conversion time"
PIXEL_DIFF: str = "Pixel-diff"

# When Python is writing JPEG file into File System,
# it will first write file under a special extension ("*.tmp-jpeg")
# After entire file is written,
# it will rename tha file to the proper extension ("*.jpeg")
# Purpose of this logic is to prevent other process (e.g. node.js)
# from reading a partially written jpeg file

_EXT_TMP_JPEG: str = ".tmp-jpg"

# Time between heartbeat images: 5 minutes
_HEARTBEAT_TIME: int = 300


class MotionSettings:
    def __init__(self, capture_count: int = 3, capture_gap: int = 1,
                 threshold: int = 820000, heartbeat: bool = False) -> None:
        self.capture_count: int = capture_count
        self.capture_gap: int = capture_gap
        self.threshold: int = threshold
        self.max_img_count: int = 500 - self.capture_count
        self.heartbeat: bool = heartbeat

class MotionDetection:
    def __init__(self, pi_camera, pi_camera_settings: PiCameraSetting,      # pyre-ignore[2]
                 pi_camera_helper: PiCameraHelper, motion_settings: MotionSettings,
                 motion_images_storage_dir_name: str, auto_exposure_storage_file_path: str,
                 region_details: Optional[RegionDetails] = None) -> None:
        # Missing parameter annotation [2]: Parameter `pi_camera` has no type specified.

        self._pi_camera = pi_camera      # pyre-ignore[4]
        self._motion_settings = motion_settings
        self._pi_camera_settings = pi_camera_settings
        # set the camera rotation
        self._pi_camera.rotation: int = self._pi_camera_settings.rotation
        self._pi_camera_helper = pi_camera_helper
        self._motion_images_storage_dir_name = motion_images_storage_dir_name
        self._auto_exposure_storage_file_path = auto_exposure_storage_file_path

        self._ae_cache_reader: AutoExposureSettingReader = FileAutoExposureSettingProcessor(
            setting_directory_abs_path=self._auto_exposure_storage_file_path,
            pi_camera_setting=self._pi_camera_settings,
            fs_caching_ttl_in_second=600,    # 10 minutes
            memory_caching_ttl_in_second=30,    # 30 seconds
            pi_camera_helper=self._pi_camera_helper)
        self._motion_loop: bool = False

        self._ae_logger: AELogger = AELogger(__name__, AELoggerStyle.LOGGER)

        self._state_machine: MotionStateMachine = MotionStateMachine()
        self._state_machine.start()

        self.region_details = region_details
        self.last_image_time: float = time.time()

        self._delete_tmp_jpeg_files()

    def _capture_and_store_image(self, file_index: int = 0) -> None:
        curr_time = datetime.datetime.now().replace(microsecond=0).isoformat()
        file_name_without_ext = 'img_' + curr_time + '_' + str(file_index)
        file_name = file_name_without_ext + _EXT_TMP_JPEG
        file_path = os.path.join(self._motion_images_storage_dir_name, file_name)
        self._pi_camera.capture(file_path, format="jpeg")
        new_file_path = os.path.join(self._motion_images_storage_dir_name,
                                     file_name_without_ext + ".jpg")
        os.rename(file_path, new_file_path)

    def _delete_tmp_jpeg_files(self) -> None:
        files = os.listdir(self._motion_images_storage_dir_name)
        for file in files:
            if file.endswith(_EXT_TMP_JPEG):
                os.remove(os.path.join(self._motion_images_storage_dir_name, file))

    def _heartbeat(self) -> None:
        if self._motion_settings.heartbeat and \
                (time.time() - self.last_image_time) >= _HEARTBEAT_TIME:
            self._pi_camera.resolution: Tuple[int, int] = self._pi_camera_settings.resolution
            self._ae_logger.info_log("Sending heartbeat image")
            self._capture_and_store_image()
            self._pi_camera.resolution: Tuple[int, int] = (640, 480)

            self.last_image_time = time.time()

    def _scan_motion(self) -> bool:
        self._pi_camera.resolution: Tuple[int, int] = (640, 480)
        motion_found: bool = False
        auto_exposure_run: bool = False

        self._ae_logger.debug_timed_op_start(TAKE_PICTURE)
        data1 = self._pi_camera_helper.take_rgb_stream_np_array()
        self._ae_logger.debug_timed_op_end(TAKE_PICTURE)
        # gray1 = cv2.cvtColor(data1, cv2.COLOR_BGR2GRAY)
        while not motion_found:
            # USER NOTIFICATIONS - MotionStateMachine
            self._state_machine.safe_transition_to_ready()
            self._heartbeat()
            self._pi_camera.resolution: Tuple[int, int] = (640, 480)
            # Waiting for 0.5 second between two image captures
            time.sleep(0.5)
            self._ae_logger.debug_timed_op_start(TAKE_PICTURE)
            data2 = self._pi_camera_helper.take_rgb_stream_np_array()
            self._ae_logger.debug_timed_op_end(TAKE_PICTURE)
            # self.ae_logger.debug_timed_op_start(GRAYSCALE_CONVERSTION)
            # gray2 = cv2.cvtColor(data2, cv2.COLOR_BGR2GRAY)
            #self.ae_logger.debug_timed_op_end(GRAYSCALE_CONVERSTION)
            self._ae_logger.debug_timed_op_start(PIXEL_DIFF)
            diff_count = np.count_nonzero(data2-data1)
            self._ae_logger.debug_timed_op_end(PIXEL_DIFF)
            self._ae_logger.debug_log("Pixel diff is : {}".format(diff_count))
            if diff_count > self._motion_settings.threshold and not auto_exposure_run:
                motion_found = True
            else:
                data1 = data2
                auto_exposure_run = False

            # USER NOTIFICATIONS - MotionStateMachine
            # ONLY IF not in cache
            if self.region_details:
                ae_settings = self._ae_cache_reader.read_region_auto_exposure_setting(
                    self.region_details,
                    functools.partial(self._state_machine.safe_transition_to_ae))
            else:
                ae_settings = self._ae_cache_reader.read_default_region_auto_exposure_setting(
                    callback_on_cache_miss=functools.partial(
                        self._state_machine.safe_transition_to_ae
                    ))
            self._ae_logger.info_log("ae_settings.shutter_speed={}"
                                     .format(ae_settings.shutter_speed))

            if self._state_machine.get_cur_state() == STATE_AE:
                auto_exposure_run = True
            # 'exposure loop' disables the 'auto' awb_mode
            # Re-Enabling the 'auto' awb_mode after running the exposure loop
            self._pi_camera.awb_mode: str = 'auto'
        return motion_found

    def start_motion_detection(self) -> None:
        self._motion_loop = True
        self._detect_motion()

    def stop_motion_detection(self) -> None:
        self._motion_loop = False

    def capture_sequence(self) -> None:
        self._pi_camera.resolution: Tuple[int, int] = self._pi_camera_settings.resolution
        # Retrieving a list of files present in the directory
        self._ae_logger.info_log('Capture_sequence: shutter_speed={} '
                                 'exposure_speed={} awb_gains={}'
                                 .format(self._pi_camera.shutter_speed,
                                         self._pi_camera.exposure_speed,
                                         self._pi_camera.awb_gains))
        files_list = os.listdir(self._motion_images_storage_dir_name)

        # Delete older files to before we store new ones
        # If more than max_img_count files are present in the directory
        if len(files_list) > (self._motion_settings.max_img_count):
            files_list = sorted(files_list)
            while len(files_list) > (self._motion_settings.max_img_count):
                os.remove(os.path.join(self._motion_images_storage_dir_name, files_list.pop(0)))

        for j in range(0, self._motion_settings.capture_count):

            self._capture_and_store_image(j)

            time.sleep(self._motion_settings.capture_gap)

        self.last_image_time = time.time()


    def _detect_motion(self) -> None:
        self._ae_logger.info_log("Scanning for Motion threshold={}"
                                 .format(self._motion_settings.threshold))
        while self._motion_loop:
            if self._scan_motion():
                self._ae_logger.info_log("Motion detected")
                self._ae_logger.info_log("capture_sequence about to start")

                # USER NOTIFICATIONS - MotionStateMachine
                self._state_machine.safe_transition_to_capture()

                # capture sequence will require time equal to
                # MotionSettings.capture_gap * MotionSettings.capture_count
                # By default this is about 6 seconds
                self.capture_sequence()
                self._ae_logger.info_log("capture_sequence completes")

                # USER NOTIFICATIONS - MotionStateMachine
                self._state_machine.safe_transition_to_notify()
