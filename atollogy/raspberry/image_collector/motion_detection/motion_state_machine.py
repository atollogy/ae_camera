
# pyre-strict

from enum import Enum
import os
import logging
from logging.handlers import RotatingFileHandler
from typing import Awaitable, Callable, Dict, List, Tuple, Optional, cast
from types import FrameType             # Signal Handler Annotation
import signal
import sys

from atollogy.common.state_machine.single_state_machine import \
        ActionContext, SingleStateMachine, TransitionDef, StateDef

class LEDColor(Enum):
    """These values are based on testing with blinkt strips;
     they should be more true to the eye than picking RGB values"""
    # RED: Tuple[int, int, int] = (255, 0, 0)
    # WHITE: Tuple[int, int, int] = (255, 196, 128)
    YELLOW: Tuple[int, int, int] = (255, 128, 0)
    GREEN: Tuple[int, int, int] = (0, 255, 0)
    BLUE: Tuple[int, int, int] = (0, 0, 255)
    NONE: Tuple[int, int, int] = (0, 0, 0)

# Controls whether MotionStateMachine will exhibit visible behavior on a machine
# without blinkt.
DEBUG_DISPLAY: bool = False

_COLOR_TRANSLATION: Dict[LEDColor, str] = {
    LEDColor.YELLOW: '[ (Y) ( ) ]',
    LEDColor.GREEN:  '[ ( ) (G) ]',
    LEDColor.BLUE:   '[ (B) ( ) ]',
    LEDColor.NONE:   '[ ( ) ( ) ]',
}

class LEDCallCounter:
    _led_call_stat: Dict[LEDColor, int] = {
        LEDColor.YELLOW: 0,
        LEDColor.GREEN:  0,
        LEDColor.BLUE:   0,
        LEDColor.NONE:   0,
    }
  # assuming the state machine will have only a single thread for now
    def inc_count(self, led_color: LEDColor) -> None:
        self._led_call_stat[led_color] += 1

_TEST_LED_CALL_COUNTER: Optional[LEDCallCounter] = None
BLINKT_EXISTS: bool = False

# motion state machine should attempt to load blinkt, otherwise function normally
# this would occur where we haven't installed blinkt, where there isn't LED hardware,
# or on development computers
try:
    import blinkt       # pylint: disable=import-error  # pyre-ignore[21]
    # Unable to import 'blinkt' (import-error)
    #Undefined import [21]: Could not find a module corresponding to import `blinkt`.
    BLINKT_EXISTS: bool = True

    def handler(signum: int, frame: FrameType) -> None: # pylint: disable=unused-argument
    # Unused argument 'signum', 'frame' (unused-argument)
        blinkt.clear()
        blinkt.show()
        sys.exit()

    # SIGTERM and SIGHUP are handled to deal with stopping the service
    # since systemctl stop sends these signals. SIGINT should handle other
    # 'killing' of the process
    signal.signal(cast(int, signal.SIGTERM), handler)
    signal.signal(cast(int, signal.SIGHUP), handler)
    signal.signal(cast(int, signal.SIGINT), handler)

except ModuleNotFoundError:
    _TEST_LED_CALL_COUNTER: LEDCallCounter = LEDCallCounter()

def _test_clear_cur_line() -> None:
    print(chr(27) + "[2K\r", end='', flush=True)

def _test_print_without_newline(input_str: str) -> None:
    print(input_str, end='', flush=True)

def _set_led_safe(color: LEDColor) -> None:
    """Handles setting LED safely so non-pi machines may still execute the code"""
    # originally _set_led_safe(led_position: int, color: LEDColor) -> None
    if BLINKT_EXISTS:
        # blinkt.set_pixel(led_position, *color.value)
        blinkt.set_all(*color.value, brightness=0.1)
        blinkt.show()
    else:
        cast(LEDCallCounter, _TEST_LED_CALL_COUNTER).inc_count(color)

    if DEBUG_DISPLAY:
        _test_clear_cur_line()
        _test_print_without_newline(_COLOR_TRANSLATION[color])


# Default Durations
DEFAULT_DURATION: float = 1.0
NOTIFICATION_DURATION: float = 3.0
BLINK_DURATION: float = 0.5
FAST_BLINK_DURATION: float = 0.3
SLOW_BLINK_DURATION: float = 0.7

class LEDAction:
    def __init__(self, _color: LEDColor, _duration: float = DEFAULT_DURATION) -> None:
        self.color: LEDColor = _color
        self.duration: float = _duration

    def __repr__(self) -> str:
        return "LEDAction({}, {})".format(self.color, self.duration)

def generate_led_sequence(
        action_list: List[LEDAction]) -> Callable[[ActionContext], Awaitable[None]]:
    """generates an LED notification pattern for use as a state machine action
        action_list - should be a list of LEDActions which will be repeated
        (removed) led_position - LED to be manipulated with blinkt if available"""

    async def _led_sequence(action_context: ActionContext) -> None:
        while not action_context.state_machine_has_been_shutdown() and \
                not action_context.get_new_state():
            for action in action_list:
                _set_led_safe(action.color)
                # We do not use asyncio.sleep because while that does delay the loop,
                # it also delays the transition to the state in an unpredictable way
                # which can cause the state to not switch until after
                await action_context.wait_for_new_state(action.duration)
                if action_context.get_new_state():
                    break

        action_context.mark_action_completed()

    return cast(Callable[[ActionContext], Awaitable[None]], _led_sequence)
    # This handles pyre disliking closures

STATE_STARTUP: str = "startup"
STATE_READY: str = "ready"
STATE_AE: str = "wait_on_ae"
STATE_CAPTURE: str = "wait_on_capture"
STATE_NOTIFY: str = "notify"

class MotionStateMachine(SingleStateMachine):
    def __init__(self) -> None:
        startup_state_def = StateDef(STATE_STARTUP)
        startup_state_def.action = generate_led_sequence([
            # Nothing lit up
            LEDAction(LEDColor.NONE, DEFAULT_DURATION)
        ])
        startup_state_def.transitions[STATE_READY] = TransitionDef()

        ready_state_def = StateDef(STATE_READY)
        ready_state_def.action = generate_led_sequence([
            # Solid Green light
            LEDAction(LEDColor.GREEN, DEFAULT_DURATION)
        ])
        ready_state_def.transitions[STATE_AE] = TransitionDef()
        ready_state_def.transitions[STATE_CAPTURE] = TransitionDef()

        wait_on_ae_state_def = StateDef(STATE_AE)
        wait_on_ae_state_def.action = generate_led_sequence([
            # Solid Green light
            LEDAction(LEDColor.GREEN, DEFAULT_DURATION)
        ])
        wait_on_ae_state_def.transitions[STATE_CAPTURE] = TransitionDef()
        wait_on_ae_state_def.transitions[STATE_READY] = TransitionDef()

        wait_on_capture_state_def = StateDef(STATE_CAPTURE)
        wait_on_capture_state_def.action = generate_led_sequence([
            # Blinking Green Light
            LEDAction(LEDColor.GREEN, BLINK_DURATION),
            LEDAction(LEDColor.NONE, BLINK_DURATION),
        ])
        wait_on_capture_state_def.transitions[STATE_NOTIFY] = TransitionDef()

        notify_state_def = StateDef(STATE_NOTIFY)
        notify_state_def.action = generate_led_sequence([
            # Solid Green Light
            LEDAction(LEDColor.GREEN, NOTIFICATION_DURATION),
        ])
        notify_state_def.transitions[STATE_READY] = TransitionDef()

        state_def_list = [
            startup_state_def,
            ready_state_def,
            wait_on_ae_state_def,
            wait_on_capture_state_def,
            notify_state_def
        ]
        super().__init__(STATE_STARTUP, state_def_list)

    def _init_logger(self, log_level: int = logging.DEBUG) -> None:
        """This overrides _init_logger in SingleStateMachine to change logging locations"""

        log_formatter = logging.Formatter(
            '%(asctime)s %(levelname)s name=%(name)s %(message)s')
        log_file_dir = "/var/lib/atollogy/ae_camera/logs/"

        os.makedirs(log_file_dir, exist_ok=True)
        log_file_loc = log_file_dir + "motion_state_machine.log"

        file_handler = RotatingFileHandler(log_file_loc, mode='a',
                                           encoding=None, delay=False,
                                           maxBytes=5 * 1024 * 1024,  # 5 MB
                                           backupCount=2)
        # we may switch to other file handlers in future,
        file_handler.setFormatter(log_formatter)
        file_handler.setLevel(log_level)

        self._logger: logging.Logger = logging.getLogger(__name__)
        self._logger.setLevel(log_level)
        self._logger.addHandler(file_handler)

    def shutdown(self) -> None:
        super().shutdown()
        if BLINKT_EXISTS:
            self._logger.info("LEDs cleared on shutdown")
            blinkt.clear()
            blinkt.show()

    def safe_transition_to_ready(self) -> None:
        if not self.get_cur_state() == STATE_READY:
            self.transit_to_new_state(STATE_READY)
        else:
            self._logger.warning("Transitioning to ready while already ready!")


    def safe_transition_to_ae(self) -> None:
        if not self.get_cur_state() == STATE_AE:
            self.transit_to_new_state(STATE_AE)
        else:
            self._logger.warning("Transitioning to ae while waiting on ae!")

    def safe_transition_to_capture(self) -> None:
        if not self.get_cur_state() == STATE_CAPTURE:
            self.transit_to_new_state(STATE_CAPTURE)
        else:
            self._logger.warning("Transitioning to capturing while capturing!")

    def safe_transition_to_notify(self) -> None:
        if not self.get_cur_state() == STATE_NOTIFY:
            self.transit_to_new_state(STATE_NOTIFY)
        else:
            self._logger.warning("Transitioning to notify while notifying!")
