
# pyre-strict

import os
from threading import RLock
from typing import Optional, Dict, Any, cast, Callable
import json

from atollogy.raspberry.image_collector.auto_exposure.ae_logger import AELoggerStyle, AELogger

from atollogy.raspberry.image_collector.auto_exposure.ae_detection \
    import PiCameraHelper, PiCameraSetting
from atollogy.raspberry.image_collector.auto_exposure.auto_exposure_setting_reader \
    import AutoExposureSettingReader, AutoExposureSetting, RegionDetails

class SettingCacheEntry:
    def __init__(self, read_from_fs_ts: int, ae_setting: AutoExposureSetting) -> None:
        # read_from_fs_ts is expressed in Epoch Second
        self.read_from_fs_ts = read_from_fs_ts
        self.ae_setting = ae_setting

class FileAutoExposureSettingProcessor(AutoExposureSettingReader):

    DEFAULT_REGION_NAME = "__default_region__"      # pyre-ignore[4]
    # Missing attribute annotation [4]:
    # Attribute `...` of class `...` has type `...` but no type is specified.

    def __init__(self, setting_directory_abs_path: str,
                 pi_camera_setting: PiCameraSetting,
                 fs_caching_ttl_in_second: int,
                 memory_caching_ttl_in_second: int,
                 pi_camera_helper: PiCameraHelper,
                 current_epoch_reader: Callable[[], int]
                 = AutoExposureSettingReader.get_current_time_in_epoch_second,
                 write_json_file: bool = True,
                 ae_logger_style: AELoggerStyle = AELoggerStyle.LOGGER) -> None:
        self._setting_directory_abs_path = setting_directory_abs_path
        self._pi_camera_setting = pi_camera_setting
        self._fs_caching_ttl_in_second = fs_caching_ttl_in_second
        self._write_json_file = write_json_file
        self._memory_caching_ttl_in_second = memory_caching_ttl_in_second

        if not os.path.isdir(setting_directory_abs_path):
            raise ValueError("'{}' is not a directory".format(setting_directory_abs_path))
        if not os.path.isabs(setting_directory_abs_path):
            raise ValueError("'{}' is not an absolute path".format(setting_directory_abs_path))

        self._pi_camera_helper = pi_camera_helper
        self._current_epoch_reader = current_epoch_reader
        self._ae_logger = AELogger(__name__, ae_logger_style)   # pyre-ignore[4]

        self._cache_dict = {}       # pyre-ignore[4]
        # Missing attribute annotation [4]:
        # Attribute `...` of class `...` has type `...` but no type is specified.

        # when migrated to Python 3.6+, the following should be used:
        # self._cache_dict: Dict[str, SettingCacheEntry]= {}

        self._cache_rlock = RLock()      # pyre-ignore[4]

    def read_default_region_auto_exposure_setting(
            self,
            optimal_brightness_value: int = 110,
            callback_on_cache_miss: Optional[Callable[[], None]] = None) -> AutoExposureSetting:
        '''
        Overriding this abstract method
        '''
        default_region = RegionDetails(
            region_name=FileAutoExposureSettingProcessor.DEFAULT_REGION_NAME,
            optimal_brightness_value=optimal_brightness_value)
        return self.read_region_auto_exposure_setting(default_region, callback_on_cache_miss)


    def read_region_auto_exposure_setting(
            self,
            region_details: RegionDetails,
            callback_on_cache_miss: Optional[Callable[[], None]] = None) -> AutoExposureSetting:
        '''
        Overriding this abstract method
        '''
        return self._read_from_memory_cache(region_details, callback_on_cache_miss)


    def _detect_shutter_speed(self, region: Optional[RegionDetails]) -> int:
        return self._pi_camera_helper.detect_ae_shutter_speed(self._pi_camera_setting,
                                                              region_obj=region)

    def _debug_log(self, msg_str: str) -> None:
        self._ae_logger.debug_log(msg_str)

    def _read_from_memory_cache(
            self,
            region: RegionDetails,
            callback_on_cache_miss: Optional[Callable[[], None]] = None) -> AutoExposureSetting:
        if region.region_name:
            region_name = region.region_name
        else:
            raise ValueError("region_name is not defined")

        entry = cast(Optional[SettingCacheEntry], self._cache_dict.get(region_name))
        current_time = self._current_epoch_reader()
        if entry:
            if (current_time - entry.read_from_fs_ts
                    <= self._memory_caching_ttl_in_second):
                if (current_time - entry.ae_setting.last_detection_time
                        <= self._fs_caching_ttl_in_second):
                    return entry.ae_setting

        with self._cache_rlock:
            ae_setting = self._read_setting_json_file(region)

            if not ae_setting:
                if callback_on_cache_miss:
                    try:
                        callback_on_cache_miss()
                    except Exception as exception:  #pylint: disable=broad-except
                        self._debug_log(
                            "Exception occured in callback_on_cache_miss {}".format(exception))
                shutter_speed = self._detect_shutter_speed(region)
                ae_setting = AutoExposureSetting(
                    last_detection_time=self._current_epoch_reader(),
                    region_details=region,
                    shutter_speed=shutter_speed)
                if self._write_json_file:
                    self._write_setting_json_file(ae_setting)

            entry = SettingCacheEntry(read_from_fs_ts=self._current_epoch_reader(),
                                      ae_setting=ae_setting)

            self._cache_dict[region_name] = entry           # pyre-ignore[5]
            # Missing global annotation [5]:
            # Globally accessible variable `self._cache_dict.__getitem__.(...)`
            # has type `SettingCacheEntry` but no type is specified.

            return entry.ae_setting

    def _read_setting_json_file(self,
                                region: RegionDetails) -> Optional[AutoExposureSetting]:

        if region.region_name:
            region_name = region.region_name
        else:
            raise ValueError("region_name is not defined")

        filename = os.path.join(self._setting_directory_abs_path,
                                "ae-setting-{}.json".format(region_name))
        try:
            with open(filename) as json_file:
                try:
                    json_dict = cast(Dict[str, Any], json.load(json_file))
                except ValueError as value_error:
                    # JSON format error
                    self._debug_log("JSON Format Error: {}".format(value_error))
                    return None

                if not isinstance(json_dict, dict):
                    msg = "Excepting a JSON object but got {}; discarding content from {}"\
                        .format(type(json_dict), filename)
                    self._debug_log(msg)
                    return None

                setting = self._conv_json_dict_to_setting_obj(
                    cast(Dict[str, Any], json_dict))
                # Incompatible parameter type [6]: Expected `Dict[str, typing.Any]`
                # for 1st anonymous parameter to call ... but got `Dict[typing.Any, typing.Any]`.

                if not ((self._current_epoch_reader() - setting.last_detection_time)
                        <= self._fs_caching_ttl_in_second):
                    self._debug_log("Cache expired at file: {}".format(filename))
                    return None

                if not setting.region_details == region:
                    self._debug_log(
                        "Region details mismatched. Discarding content at file: {}".format(
                            filename))
                    return None

                return setting

        except FileNotFoundError:
            self._debug_log("Cache not found at file: {}".format(filename))
            return None

    def _write_setting_json_file(self, ae_setting: AutoExposureSetting) -> None:
        if ae_setting.region_details and ae_setting.region_details.region_name:
            region_name = ae_setting.region_details.region_name
        else:
            raise ValueError("region_name is not defined")

        filename = os.path.join(self._setting_directory_abs_path,
                                "ae-setting-{}.json".format(region_name))

        with open(filename, mode="w+") as json_file:
            json_dict = ae_setting.__dict__.copy()
            json_dict["region_details"] = ae_setting.region_details.__dict__.copy()
            json_file.write(json.dumps(json_dict, indent=4))

        self._debug_log("Auto Exposure Setting saved at file: {}".format(filename))


    @staticmethod
    def _conv_json_dict_to_setting_obj(
            json_dict: Dict[str, Any]) -> AutoExposureSetting:   #pyre-ignore[2]
        # Missing parameter annotation [2]: Parameter `json_dict` must have a
        # type that does not contain `Any`.
        region_details_dict = cast(Dict[str, Any], json_dict.get("region_details"))
        if region_details_dict:
            json_dict["region_details"] = \
                FileAutoExposureSettingProcessor._conv_json_dict_to_region_obj(
                    region_details_dict)
        return AutoExposureSetting(**json_dict)

    @staticmethod
    def _conv_json_dict_to_region_obj(json_dict: Dict[str, Any]) -> RegionDetails:   #pyre-ignore[2]
        # Missing parameter annotation [2]: Parameter `json_dict`
        # must have a type that does not contain `Any`.
        return RegionDetails(**json_dict)
