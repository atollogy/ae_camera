
# pyre-strict

import calendar
import time
from abc import ABC, abstractmethod
from typing import Callable, Optional, Type


def auto_str(cls: Type):      #pyre-ignore
    # Missing parameter annotation [2]: Parameter `cls` must have a
    # type that does not contain `Any`.
    def __str__(self) -> str:       # pyre-ignore[2]
        # Missing parameter annotation [2]: Parameter `self` has no type specified.
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items())
        )
    cls.__str__ = __str__
    return cls


@auto_str
class RegionDetails:

    # the following syntax is supported by Python 3.6+
    '''
    region_name: str
    min_x: int
    min_y: int
    width: int
    height: int
    optimal_brightness_value: int
    '''

    def __init__(self, region_name: str,
                 min_x: Optional[int] = None, min_y: Optional[int] = None,
                 width: Optional[int] = None, height: Optional[int] = None,
                 optimal_brightness_value: Optional[int] = None) -> None:
        self.region_name = region_name

        if optimal_brightness_value:
            self.optimal_brightness_value = optimal_brightness_value
        else:
            self.optimal_brightness_value = 110

        # when min_x, min_y, width, height are not specified (i.e. None),
        # auto detection logic will use the default region

        self.min_x = min_x
        self.min_y = min_y
        self.width = width
        self.height = height

    def __eq__(self, other) -> bool:        # pyre-ignore[2]
        #  Missing parameter annotation [2]: Parameter `other` has no type specified.
        if isinstance(other, RegionDetails):
            return  (self.region_name == other.region_name and
                     self.optimal_brightness_value == other.optimal_brightness_value and
                     self.min_x == other.min_x and
                     self.min_y == other.min_y and
                     self.width == other.width and
                     self.height == other.height)

        return False

    def valid(self) -> bool:

        if self.optimal_brightness_value:
            brightness_int_val = int(self.optimal_brightness_value)
            if not 0 <= brightness_int_val < 256:
                return False

        '''Either no region is defined (min_x, min_y, width, height) are None
        or all four values must be defined'''
        if not (self.min_x and self.min_y and self.width and self.height):
            return (self.min_x is None and
                    self.min_y is None and
                    self.width is None and
                    self.height is None)

        return (self.min_x >= 0 and
                self.min_y >= 0 and
                self.width > 0 and
                self.height > 0)

@auto_str
class AutoExposureSetting:
    def __init__(self, last_detection_time: int, region_details: Optional[RegionDetails],
                 shutter_speed: int) -> None:
        self.last_detection_time = last_detection_time
        self.region_details = region_details
        self.shutter_speed = shutter_speed

    # the following syntax is supported by Python 3.6+
    '''
    last_detection_time: int
    region_details: Optional[RegionDetails]
    shutter_speed: int
    '''

class AutoExposureSettingReader(ABC):
    '''
    Concrete implementation of this abstract class will invoke
    PiCameraHelper.detect_ae_shutter_speed method,
    whenever there is a cache miss.

    Implementation may make use of File System and In-Memory
    in a combined caching methodology.

    With In-memory caching inside an implementation,
    the call rate of "read_*_auto_exposure_setting(...)" is
    expected to be 1000+ times per second.

    The explicit timestamp variants of the methods below
    are mainly used in Unit testing.
    '''

    @staticmethod
    def get_current_time_in_epoch_second() -> int:
        return int(calendar.timegm(time.gmtime()))

    @abstractmethod
    def read_default_region_auto_exposure_setting(
            self,
            optimal_brightness_value: int = 110,
            callback_on_cache_miss: Optional[Callable[[], None]] = None) -> AutoExposureSetting:
        raise NotImplementedError()

    @abstractmethod
    def read_region_auto_exposure_setting(
            self,
            region_details: RegionDetails,
            callback_on_cache_miss: Optional[Callable[[], None]] = None) -> AutoExposureSetting:
        raise NotImplementedError()
