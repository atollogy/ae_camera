
# pyre-strict

import sys
import time
from typing import Tuple, List, Optional, Dict, cast
import signal
import numpy as np      # pyre-ignore[21]
# Undefined import [21]: Could not find a module corresponding to import

from atollogy.raspberry.image_collector.auto_exposure.auto_exposure_setting_reader \
    import RegionDetails

from atollogy.raspberry.image_collector.auto_exposure.ae_logger \
    import AELogger, AELoggerStyle


class PiCameraSetting:
    def __init__(self, resolution: Tuple[int, int] = (480, 240), iso: int = 800,
                 starting_shutter_speed: int = 5000,
                 max_framerate: int = 20,
                 vflip: bool = False,
                 rotation: int = 0,
                 ae_loop_timeout: int = 30,
                 wb_gains: Optional[List[float]] = None) -> None:
        self.resolution: Tuple[int, int] = resolution
        self.iso: int = iso
        self.starting_shutter_speed: int = starting_shutter_speed
        self.max_framerate: int = max_framerate
        self.vflip: bool = vflip
        self.rotation: int = rotation
        self.ae_loop_timeout: int = ae_loop_timeout
        self.wb_gains: Optional[List[float]] = wb_gains


class AEResolutionHelper:

    RATIO_16_9: str = "16:9"
    RATIO_4_3: str = "4:3"
    RATIO_3_4: str = "3:4"
    RATIO_9_16: str = "9:16"
    RATIO_UNDEFINED: str = "undefined"

    _RATIO_VALUE_MAP: Dict[str, float] = {RATIO_16_9: 16 / 9, RATIO_4_3: 4 / 3,
                                          RATIO_3_4: 3 / 4, RATIO_9_16: 9 / 16}

    _RATIO_MAX_AE_RES_MAP: Dict[str, Tuple[int, int]] = {
        RATIO_16_9: (1920, 1080), RATIO_4_3: (1440, 1080),
        RATIO_3_4: (1080, 1440), RATIO_9_16: (1080, 1920)}

    @staticmethod
    def _compare_float(number: float, target_number: float, abs_epsilon: float) -> bool:
        return target_number - abs_epsilon <= number <= target_number + abs_epsilon

    @staticmethod
    def _determine_aspect_ratio(width: int, height: int) -> str:
        ratio = width / height
        for ratio_key in AEResolutionHelper._RATIO_VALUE_MAP:
            target_ratio_value = AEResolutionHelper._RATIO_VALUE_MAP[ratio_key]
            if AEResolutionHelper._compare_float(ratio, target_ratio_value, 0.02):
                return ratio_key
        raise ValueError("Undefined resolution: width={}, height{}".format(width, height))

    @staticmethod
    def reduce_resolution_for_ae_loop(orig_resolution: Tuple[int, int]) -> Tuple[int, int]:
        ratio_key = AEResolutionHelper._determine_aspect_ratio(width=orig_resolution[0],
                                                               height=orig_resolution[1])
        max_res = AEResolutionHelper._RATIO_MAX_AE_RES_MAP[ratio_key]
        if orig_resolution[0] > max_res[0]:
            sys.stderr.write("AEResolutionHelper: reduction auto-exposure resolution to: {} \n"
                             .format(max_res))
            return max_res

        return orig_resolution

class PiCameraHelper:

    _OP_PI_AUTO_EXPOSURE_LOOP: str = "pi_auto_exposure_loop"
    _OP_CAPTURE_PHOTO_OPTIMAL_EXPOSURE: str = "capture_photo_optimal_exposure"

    # scaling_values are for BGR - LUMA = 0.2126 R + 0.7152 G + 0.0722 B
    _SCALING_VALUES: List[float] = [0.072, 0.715, 0.213]

    def __init__(self, camera, aeLoggerStyle: AELoggerStyle) -> None:   # pyre-ignore[2]
        # Missing parameter annotation [2]: Parameter `camera` has no type specified.
        self._camera = camera           # pyre-ignore[4]
        # assuming this python is run in a single threaded fashion
        # For this dictionary, the key is Operation Name and value is time in Epoch millisecond

        self.ae_logger: AELogger = AELogger(__name__, aeLoggerStyle)


    def init_camera_setting(self, pi_camera_setting: PiCameraSetting,
                            given_resolution: Optional[Tuple[int, int]] = None,
                            given_shutter_speed: Optional[int] = None) -> None:

        # These two "given_*" parameters would override setting stored inside "pi_camera_setting"

        self._camera.vflip: bool = pi_camera_setting.vflip
        self._camera.framerate: int = pi_camera_setting.max_framerate
        self._camera.rotation: int = pi_camera_setting.rotation

        if given_resolution:
            self._camera.resolution: Tuple[int, int] = given_resolution
        else:
            self._camera.resolution: Tuple[int, int] = pi_camera_setting.resolution

        self._camera.iso: int = pi_camera_setting.iso

        if given_shutter_speed:
            self._camera.shutter_speed: int = given_shutter_speed
        else:
            self._camera.shutter_speed: int = pi_camera_setting.starting_shutter_speed

        time.sleep(2)
        # TODO: to think about how to reduce this camera setup wait period

        if pi_camera_setting.wb_gains is None:
            wb_gains = self._camera.awb_gains
        else:
            wb_gains = pi_camera_setting.wb_gains

        self._camera.awb_mode: str = 'off'
        self._camera.awb_gains = wb_gains

    def take_rgb_stream_np_array(self)-> np.ndarray:        # pyre-ignore[11]
        #  Undefined type [11]: Type `np.ndarray` is not defined.
        raise NotImplementedError()

    def detect_ae_shutter_speed(
            self, pi_camera_setting: PiCameraSetting,
            region_obj: Optional[RegionDetails] = None) -> int:

        ae_resolution = AEResolutionHelper.reduce_resolution_for_ae_loop(
            pi_camera_setting.resolution)

        self.init_camera_setting(pi_camera_setting, given_resolution=ae_resolution)

        self.ae_logger.debug_timed_op_start(PiCameraHelper._OP_PI_AUTO_EXPOSURE_LOOP)

        # This is needed to bring the resolution to the right block size for
        # the camera to deal with
        # This is also an annoying artifact of picamera and one of the things
        # I am not a fan of
        width, height = PiCameraHelper._pad(*ae_resolution)  # PiResolution(resolution).pad()


        if region_obj:
            if not PiCameraHelper._validate_region(region_obj, pi_camera_setting.resolution):
                raise ValueError('Invalid region provided: {}'.format(region_obj))

            if (region_obj.min_x and region_obj.min_y
                    and region_obj.width and region_obj.height):
                region = (region_obj.min_x, region_obj.min_x+region_obj.width,
                          region_obj.min_y, region_obj.min_y+region_obj.height)
            else:
                region = None
            optimal_brightness_value: int = cast(int, region_obj.optimal_brightness_value)
        else:
            region = None
            optimal_brightness_value: int = 110


        '''region = (xmin, xmax, ymin, ymax)
            region will be centered on the image if it is not set manually'''
        if not region:
            size = min(width, height, 200)
            ae_region = ((width - size) // 2, (width + size) // 2,
                         (height - size) // 2, (height + size) // 2)
            self.ae_logger.info_log('default ae_region={}'.format(ae_region))
        else:
            if not ae_resolution == pi_camera_setting.resolution:
                reduction_factor = ae_resolution[0] / pi_camera_setting.resolution[0]
                ae_region = (int(region[0] * reduction_factor), int(region[1] * reduction_factor),
                             int(region[2] * reduction_factor), int(region[3] * reduction_factor))
                self.ae_logger.info_log('reduced ae_region={}'.format(ae_region))
            else:
                ae_region = region

        frame = np.zeros(width * height * 3, dtype=np.uint8)
        frame = frame.reshape(width, height, 3)

        '''Timeout if auto exposure loop doesn't reach a value in given time'''

        def __alarm_handler(number, frame):     # pyre-ignore[2]
            # Missing parameter annotation [2]: Parameter `*` has no type specified.
            raise TimeoutError('auto_exposure_loop timed out: number={} type(frame)={} '
                               .format(number, type(frame)))

        signal.signal(signal.SIGALRM, __alarm_handler)
        signal.alarm(pi_camera_setting.ae_loop_timeout)

        self.ae_logger.info_log('Exposure_Loop: before loop: shutter_speed={} '
                                'exposure_speed={} awb_gains={}'
                                .format(self._camera.shutter_speed,
                                        self._camera.exposure_speed,
                                        self._camera.awb_gains))

        try:
            self._pi_auto_exposure_loop(ae_region, frame,
                                        optimal_brightness_value=optimal_brightness_value)
        except TimeoutError:
            self.ae_logger.info_log('Exposure_Loop: Timed Out\n')

        signal.alarm(0)  # Disable the alarm

        self.ae_logger.debug_timed_op_end(PiCameraHelper._OP_PI_AUTO_EXPOSURE_LOOP)

        self.ae_logger.info_log('Exposure_Loop: after loop: shutter_speed={} '
                                'exposure_speed={} awb_gains={}'
                                .format(self._camera.shutter_speed,
                                        self._camera.exposure_speed,
                                        self._camera.awb_gains))

        # now setting back to original resolution if needed
        if not ae_resolution == pi_camera_setting.resolution:
            self._camera.resolution: Tuple[int, int] = pi_camera_setting.resolution
            time.sleep(0.25)
            # not 100% that we need to sleep for 0.25 seconds,
            # after re-adjusting the resolution to requested resolution.

        return self._camera.shutter_speed

    @staticmethod
    def _pad(width: int, height: int, cwidth: int = 32, cheight: int = 16) -> Tuple[int, int]:
        '''PiCamera Capture outputs rgb data in blocks from the GPU,
        this will pad a width and height to the right size'''
        return (((width + (cwidth - 1)) // cwidth) * cwidth,
                ((height + (cheight - 1)) // cheight) * cheight)

    @staticmethod
    def _validate_region(region: RegionDetails, resolution: Tuple[int, int]) -> bool:
        '''checks that the region is completely within the given resolution'''

        # if the region doeesn't have any values, check if it is valid
        if not (region.min_x and region.min_y
                and region.width and region.height):
            return region.valid()

        min_x = region.min_x
        max_x = region.min_x + region.width
        min_y = region.min_y
        max_y = region.min_y + region.height

        width, height = resolution

        valid_x = (0 <= min_x < max_x <= width)
        valid_y = (0 <= min_y < max_y <= height)

        # region.valid is used to check the optimal brightness is within [0, 255]
        return (valid_x and valid_y) and region.valid()

    def _pi_auto_exposure_loop(self, region: Tuple[int, int, int, int],
                               frame: np.array,     #pyre-ignore[11]
                               optimal_brightness_value: int = 110,
                               cuttoff_threshold: float = 1.5, max_brightness_value: int = 256,
                               max_exposure_time: Optional[int] = None) -> int:
                               # Undefined type [11]: Type `np.array` is not defined.
        '''Loop until optimal exposure is reached
            camera: picamera.PiCamera object
            region: [Xmin, Xmax, Ymin, Ymax]
            frame: numpy.array
            optimal_brightness_value: int
            cuttoff_threshold: float
            max_brightness_value: int
            max_exposure_time int
            '''


        # NOTE: Warnings and more intelligent solution (ie change framerate
        # to allow max shutter speed if specified)
        if max_exposure_time:
            max_exposure_time = min(1000000 // cast(int, self._camera.framerate),
                                    max_exposure_time)
        else:
            max_exposure_time = 1000000 // cast(int, self._camera.framerate)


        black_threshold: int = 2

        # exposure is locked the real value of the camera exposure with each loop.
        # separating them provides some freedom but introduces large fluctuation
        current_exposure_time: int = self._camera.exposure_speed

        while True:
            self.ae_logger.debug_timed_op_start(PiCameraHelper._OP_CAPTURE_PHOTO_OPTIMAL_EXPOSURE)
            self._camera.capture(frame, 'bgr', use_video_port=True)
            self.ae_logger.debug_timed_op_end(PiCameraHelper._OP_CAPTURE_PHOTO_OPTIMAL_EXPOSURE)

            # Select region of interest for exposure work
            region_of_interest = frame[region[0]:region[1], region[2]:region[3]]

            # Get average luma value for selection
            region_of_interest_lum = np.dot(
                region_of_interest, PiCameraHelper._SCALING_VALUES)
            current_brightness_value: float = np.average(region_of_interest_lum)

            self.ae_logger.debug_log("Exposure_Loop: current_brightness_value={}"
                                     .format(current_brightness_value))

            if abs(optimal_brightness_value - current_brightness_value) < cuttoff_threshold:

                self.ae_logger.info_log("Exposure_Loop: completed with current_brightness_value "
                                        +"within optimal_brightness_value threshold range")

                return current_exposure_time

            '''Calculate Change and Next Exposure'''
            alpha = (max_brightness_value - optimal_brightness_value)*max_exposure_time
            next_exposure_time: float = \
                (((max_brightness_value - current_brightness_value)
                  *current_exposure_time*max_exposure_time) /
                 ((optimal_brightness_value - current_brightness_value)
                  *current_exposure_time + alpha))

            # This is a little hacky - but necessary
            # When adapting to substantially brighter lighting conditions, the algorithm drops
            # the exposure substantially. But then it gets stuck because the increase increment
            # is smaller than the minimum step in the pi
            # (the pi only allows certain shutter speeds)'''
            if abs(current_brightness_value) < black_threshold:
                next_exposure_time += 100

            '''set exposure, keeping it to a whole number'''
            next_exposure_time = max(int(next_exposure_time), 1)
            self._camera.shutter_speed: int = next_exposure_time


            # camera shutter_speed and exposure speed are not equal contrary to documentation.
            # exposure_speed is limited to certain discrete values, and may not instantly
            # reflect large changes this fixes current_exposure_time to exposure speed,
            # preventing oscillation and desync of these values specifically this is important
            # when achieving optbrightness values higher or lower than normal
            current_exposure_time = cast(int, self._camera.exposure_speed)

            if not current_exposure_time == next_exposure_time:
                self.ae_logger.debug_log(
                    ("Exposure_Loop: setting shutter speed with different results: "+
                     "retrieved-exposure_speed:{} not-eq setting-shutter_speed:{}").format(
                         current_exposure_time, next_exposure_time))
