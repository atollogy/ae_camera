
import sys
import time
import traceback
from enum import Enum
import logging
from logging.handlers import RotatingFileHandler
from typing import cast
import os

class AELoggerStyle(Enum):
    STD_ERR = 1
    LOGGER = 2

class AELogger:
    '''
    This AELogger is NOT thread-safe when it comes to time-measurement purpose.
    There is a single instance of Dictionary that keep tracks of
    Start Time and End Time of an Operation.

    '''

    def __init__(self, name: str, style: AELoggerStyle, log_level: int = logging.DEBUG) -> None:

        if not name:
            raise ValueError("undefined value in 'name' parameter")

        self._name = name
        self._style = style

        if style == AELoggerStyle.LOGGER:
            log_formatter = logging.Formatter(
                '%(asctime)s %(levelname)s name=%(name)s func=%(cal_func_name)s %(message)s')

            log_file_dir = "/var/lib/atollogy/ae_camera/logs/"
            # hardcoded log file location for now

            os.makedirs(log_file_dir, exist_ok=True)
            log_file_loc = log_file_dir + "ae_logger.log"

            file_handler = RotatingFileHandler(log_file_loc, mode='a',
                                               maxBytes=5 * 1024 * 1024,  # 5 MB
                                               backupCount=2, encoding=None, delay=False)
            # we may switch to other file handlers in future,
            # depending on how we manage log files at Raspberry Pi devices

            file_handler.setFormatter(log_formatter)
            file_handler.setLevel(log_level)

            self._logger = logging.getLogger(self._name)
            self._logger.setLevel(log_level)
            # hardcoded log level for now

            self._logger.addHandler(file_handler)

        self._op_time_dict = {}

    @staticmethod
    def _cal_function_name() -> str:
        trace_stack = []
        trace_stack.extend(traceback.extract_stack(limit=5))
        trace_stack.reverse()

        for trace_elem in trace_stack:
            # print("trace_elem.name={} trace_elem.filename={}"
            #       .format(trace_elem.name, trace_elem.filename))
            if not str(trace_elem.filename).endswith("/ae_logger.py"):
                return trace_elem.name
        return ""

    def debug_log(self, msg_str: str) -> None:
        if self._style == AELoggerStyle.LOGGER:
            debug_msg = "ppid={} pid={} {}".format(
                os.getppid(), os.getpid(), msg_str)
            extra = {"cal_func_name" : AELogger._cal_function_name()}
            self._logger.debug(debug_msg, extra=extra)
        elif self._style == AELoggerStyle.STD_ERR:
            sys.stderr.write("{}: ppid={} pid={} {}\n".format(
                self._name, os.getppid(), os.getpid(), msg_str))
        else:
            # unreachable
            raise ValueError("undefined logging style: {}".format(self._style))

    def info_log(self, msg_str: str) -> None:

        if self._style == AELoggerStyle.LOGGER:
            info_msg = "ppid={} pid={} {}".format(
                os.getppid(), os.getpid(), msg_str)
            extra = {"cal_func_name": AELogger._cal_function_name()}
            self._logger.info(info_msg, extra=extra)
        elif self._style == AELoggerStyle.STD_ERR:
            sys.stderr.write("{}: ppid={} pid={} {}\n".format(
                self._name, os.getppid(), os.getpid(), msg_str))
        else:
            # unreachable
            raise ValueError("undefined logging style: {}".format(self._style))

    def debug_timed_op_start(self, op_name_str: str) -> None:
        self.debug_log(op_name_str + " started")
        self._op_time_dict[op_name_str] = int(round(time.time()*1000))
        # Missing global annotation [5]: Globally accessible variable
        # `self._op_time_dict.__getitem__.(...)` has type `int` but no type is specified.

    def debug_timed_op_end(self, op_name_str: str) -> None:
        start_time = cast(int, self._op_time_dict.get(op_name_str))
        if start_time:
            cur_time = int(round(time.time()*1000))
            time_elapsed = cur_time - start_time
            self.debug_log(op_name_str + " ended ; time_elapsed={}".format(time_elapsed))
        else:
            self.debug_log(op_name_str + " ended")
