
import os
from typing import Optional, Callable

from atollogy.raspberry.image_collector.auto_exposure.ae_detection import PiCameraSetting, PiCameraHelper
from atollogy.raspberry.image_collector.auto_exposure.ae_logger import AELoggerStyle
from atollogy.raspberry.image_collector.auto_exposure.auto_exposure_setting_reader import RegionDetails, \
    AutoExposureSetting
from atollogy.raspberry.image_collector.auto_exposure.file_auto_exposure_setting_processor import \
    FileAutoExposureSettingProcessor


class PiCameraHelperForTesting(PiCameraHelper):

    _call_count = 0

    def __init__(self, camera) -> None:
        PiCameraHelper.__init__(self, camera, AELoggerStyle.STD_ERR)

    def detect_ae_shutter_speed(
            self, pi_camera_setting: PiCameraSetting,
            region_obj: Optional[RegionDetails] = None) -> int:
        self._call_count = self._call_count + 1
        return 12345 + self._call_count * 10

class TestFileAutoExposureSettingProcessor:

    def prepare_test_reader(self, cur_epoch_reader_func: Callable[[],int],
                            write_json_file: bool = True,
                            data_dir: str = os.path.join(os.getcwd(), "tests_data")
                            ) -> FileAutoExposureSettingProcessor:
        reader = FileAutoExposureSettingProcessor(
            setting_directory_abs_path=data_dir,
            pi_camera_setting=PiCameraSetting(),
            fs_caching_ttl_in_second=300,       # 5 minutes
            memory_caching_ttl_in_second=60,    # 1 minute
            pi_camera_helper=PiCameraHelperForTesting(None),
            current_epoch_reader=cur_epoch_reader_func,
            write_json_file=write_json_file)
        return reader

    def test_default_region_call_count(self):

        test_cur_time = 1530000090
        def cur_epoch_reader_func() -> int:
            return test_cur_time

        reader = self.prepare_test_reader(cur_epoch_reader_func, write_json_file=False)

        setting_01 = reader.read_default_region_auto_exposure_setting()
        assert 12355==setting_01.shutter_speed
        assert 1 == reader._pi_camera_helper._call_count

        test_cur_time = 1530000120
        # only elapsed 30 seconds; memory cache still valid
        setting_02 = reader.read_default_region_auto_exposure_setting()
        assert 12355==setting_01.shutter_speed
        assert 1 == reader._pi_camera_helper._call_count

        test_cur_time = 1530000210
        # only elapsed 120 seconds; memory cache no longer valid
        setting_03 = reader.read_default_region_auto_exposure_setting()
        assert 12365==setting_03.shutter_speed
        assert 2 == reader._pi_camera_helper._call_count

    def test_read_setting_with_existing_file(self):

        test_cur_time = 1530000090
        def cur_epoch_reader_func() -> int:
            return test_cur_time

        reader = self.prepare_test_reader(cur_epoch_reader_func, write_json_file=False)

        region_details_dict = {
            "region_name": "sample_region", "min_x": 10, "min_y": 10, "width": 100, "height": 100
        }
        region_details = RegionDetails(**region_details_dict)

        setting_obj = reader.read_region_auto_exposure_setting(region_details)
        assert 18554==setting_obj.shutter_speed

    def test_read_setting_json_file_basic(self):

        test_cur_time = 1530000090
        def cur_epoch_reader_func() -> int:
            return test_cur_time

        reader = self.prepare_test_reader(cur_epoch_reader_func, write_json_file=False)

        region_details_dict = {
            "region_name": "sample_region", "min_x": 10, "min_y": 10, "width": 100, "height": 100
        }
        region_details = RegionDetails(**region_details_dict)

        setting_obj = reader._read_setting_json_file(region_details)
        assert 18554==setting_obj.shutter_speed

        # cache expire
        test_cur_time = 1540000090
        setting_obj = reader._read_setting_json_file(region_details)
        assert None==setting_obj

        # Region details mismatched
        region_details.min_x = 11
        test_cur_time = 1530000090
        setting_obj = reader._read_setting_json_file(region_details)
        assert None == setting_obj

    def test_read_setting_json_file_negative(self):

        test_cur_time = 1530000090
        def cur_epoch_reader_func() -> int:
            return test_cur_time

        reader = self.prepare_test_reader(cur_epoch_reader_func, write_json_file=False)

        region_details_dict = {
            "region_name": "sample_region", "min_x": 10, "min_y": 10, "width": 100, "height": 100
        }
        region_details = RegionDetails(**region_details_dict)

        region_details.region_name = "invalid_json"
        # JSON Format Error: Expecting ',' delimiter: line 4 column 25 (char 97)
        setting_obj = reader._read_setting_json_file(region_details)
        assert None==setting_obj

        region_details.region_name = "mismatch_json"
        # Excepting a JSON object but got <class 'list'>;
        setting_obj = reader._read_setting_json_file(region_details)
        assert None==setting_obj

        region_details.region_name = "region_not_found"
        # Cache not found at file
        setting_obj = reader._read_setting_json_file(region_details)
        assert None==setting_obj

    def test_write_read_json_file(self, tmpdir):
        test_cur_time = 1530000090
        def cur_epoch_reader_func() -> int:
            return test_cur_time

        reader = self.prepare_test_reader(cur_epoch_reader_func, write_json_file=True, data_dir=str(tmpdir))

        region_details_dict = {
            "region_name": "pytest_dont_add_to_git_region_01",
            "min_x": 10, "min_y": 10, "width": 100, "height": 100,
            "optimal_brightness_value" : 123
        }
        region_details = RegionDetails(**region_details_dict)

        ae_setting = AutoExposureSetting(last_detection_time=test_cur_time,
                                         region_details=region_details,
                                         shutter_speed=54321)
        reader._write_setting_json_file(ae_setting)

        ae_setting_2 = reader._read_setting_json_file(region_details)

        assert 54321 == ae_setting_2.shutter_speed
        assert region_details == ae_setting_2.region_details
        assert test_cur_time == ae_setting_2.last_detection_time

        test_cur_time = 1540000090
        # fs cache expire
        ae_setting_3 = reader._read_setting_json_file(region_details)

        assert None == ae_setting_3

    def test_basic_region_cache_cycle_with_mem_and_fs(self, tmpdir):
        test_cur_time = 1530000090
        def cur_epoch_reader_func() -> int:
            return test_cur_time

        reader = self.prepare_test_reader(cur_epoch_reader_func, write_json_file=True, data_dir=str(tmpdir))

        region_details_dict = {
            "region_name": "pytest_dont_add_to_git_region_02",
            "min_x": 10, "min_y": 10, "width": 100, "height": 100,
            "optimal_brightness_value" : 123
        }
        region_details = RegionDetails(**region_details_dict)

        setting_01 = reader.read_region_auto_exposure_setting(region_details)
        assert 12355==setting_01.shutter_speed
        assert 1 == reader._pi_camera_helper._call_count

        test_cur_time = 1530000120
        # only elapsed 30 seconds; memory cache still valid
        setting_02 = reader.read_region_auto_exposure_setting(region_details)
        assert 12355==setting_01.shutter_speed
        assert 1 == reader._pi_camera_helper._call_count

        test_cur_time = 1530000210
        # only elapsed 120 seconds; memory cache no longer valid; fs cache still valid
        setting_03 = reader.read_region_auto_exposure_setting(region_details)
        assert 12355==setting_03.shutter_speed
        assert 1 == reader._pi_camera_helper._call_count

        test_cur_time = 1530000391
        # elapsed 301 seconds (i.e. > 5 minutes); memory cache no longer valid; fs cache still valid
        setting_04 = reader.read_region_auto_exposure_setting(region_details)
        assert 12365==setting_04.shutter_speed
        assert 2 == reader._pi_camera_helper._call_count

        test_cur_time = 1530000791
        # another 400 seconds (i.e. > 5 minutes); memory cache no longer valid; fs cache still valid
        setting_05 = reader.read_region_auto_exposure_setting(region_details)
        assert 12375==setting_05.shutter_speed
        assert 3 == reader._pi_camera_helper._call_count