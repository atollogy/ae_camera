from unittest.mock import Mock, patch
from math import sqrt

from atollogy.raspberry.image_collector.auto_exposure.ae_detection import PiCameraHelper, PiCameraSetting
from atollogy.raspberry.image_collector.auto_exposure.ae_logger import AELoggerStyle, AELogger
from atollogy.raspberry.image_collector.auto_exposure.auto_exposure_setting_reader import RegionDetails

import numpy as np  # pyre-ignore[21]
import pytest       # pyre-ignore[21]
# Undefined import [21]: Could not find a module corresponding to import


class MockPiCamera(Mock):
    def __init__(self, response_curve, *args, **kwargs):
        # brightness_function provides a mapping between shutter speed and image brightness
        Mock.__init__(self, *args, **kwargs)
        self.response_curve = response_curve
        self.shutter_speed = 0

    def capture(self, frame, *args, **kwargs):
        # Produce frames with average brightness based on set value
        frame.fill(self.response_curve(self.shutter_speed))

    def __getattribute__(self, name):
        # Link exposure_speed and shutter_speed variables together to emulate PiCamera
        if name == 'exposure_speed':
            return self.__getattribute__('shutter_speed')

        return Mock.__getattribute__(self, name)

    def _get_child_mock(self, **kw):
        # Mocks for attribute or functions of MockPiCamera should be normal Mocks Objects
        return Mock(**kw)


class TestPiCameraHelper:
    # Optimal brightness value based on the value set within ae_detection
    # This will need to be updated if we change it in the future
    _OPTIMAL_BRIGHTNESS_VALUE = 110
    _INTERVAL = 20

    def test_auto_exposure_correctly_exposed(self):
        pi_camera = MockPiCamera(lambda x: self._OPTIMAL_BRIGHTNESS_VALUE)
        pi_camera_helper = PiCameraHelper(pi_camera, AELoggerStyle.STD_ERR)
        pi_camera_setting = PiCameraSetting(resolution=(1920, 1080), ae_loop_timeout=1)

        pi_camera_helper.init_camera_setting(pi_camera_setting)

        starting_shutter_speed = pi_camera.exposure_speed
        ending_shutter_speed = pi_camera_helper.detect_ae_shutter_speed(pi_camera_setting)

        assert starting_shutter_speed == ending_shutter_speed


    def test_auto_exposure_under_exposed(self):
        pi_camera = MockPiCamera(lambda x: self._OPTIMAL_BRIGHTNESS_VALUE - self._INTERVAL)
        pi_camera_helper = PiCameraHelper(pi_camera, AELoggerStyle.STD_ERR)
        pi_camera_setting = PiCameraSetting(resolution=(1920, 1080), ae_loop_timeout=1)

        pi_camera_helper.init_camera_setting(pi_camera_setting)

        starting_shutter_speed = pi_camera.exposure_speed
        ending_shutter_speed = pi_camera_helper.detect_ae_shutter_speed(pi_camera_setting)

        assert starting_shutter_speed < ending_shutter_speed


    def test_auto_exposure_over_exposed(self):
        pi_camera = MockPiCamera(lambda x: self._OPTIMAL_BRIGHTNESS_VALUE + self._INTERVAL)
        pi_camera_helper = PiCameraHelper(pi_camera, AELoggerStyle.STD_ERR)
        pi_camera_setting = PiCameraSetting(resolution=(1920, 1080), ae_loop_timeout=1)

        pi_camera_helper.init_camera_setting(pi_camera_setting)

        starting_shutter_speed = pi_camera.exposure_speed
        ending_shutter_speed = pi_camera_helper.detect_ae_shutter_speed(pi_camera_setting)

        assert starting_shutter_speed > ending_shutter_speed


    def test_auto_exposure_linear_response(self):
        conversion = lambda x: 0.01*x

        pi_camera = MockPiCamera(conversion)
        pi_camera_helper = PiCameraHelper(pi_camera, AELoggerStyle.STD_ERR)
        pi_camera_setting = PiCameraSetting(resolution=(1920, 1080), ae_loop_timeout=1)
        ending_shutter_speed = pi_camera_helper.detect_ae_shutter_speed(pi_camera_setting)

        assert (self._OPTIMAL_BRIGHTNESS_VALUE - 1) <= conversion(ending_shutter_speed) \
            <= (self._OPTIMAL_BRIGHTNESS_VALUE + 1)


    def test_auto_exposure_nonlinear_concave_response(self):
        conversion = lambda x: sqrt(x)

        pi_camera = MockPiCamera(conversion)
        pi_camera_helper = PiCameraHelper(pi_camera, AELoggerStyle.STD_ERR)
        pi_camera_setting = PiCameraSetting(resolution=(1920, 1080), ae_loop_timeout=1)
        ending_shutter_speed = pi_camera_helper.detect_ae_shutter_speed(pi_camera_setting)

        assert (self._OPTIMAL_BRIGHTNESS_VALUE - 1) <= conversion(ending_shutter_speed) \
            <= (self._OPTIMAL_BRIGHTNESS_VALUE + 1)


    @pytest.mark.xfail(reason='Current auto exposure algorithm was not designed \
        to handle nonlinear convex response curves')
    def test_auto_exposure_nonlinear_convex_response(self):
        conversion = lambda x: 2**(0.001*x)

        pi_camera = MockPiCamera(conversion)
        pi_camera_helper = PiCameraHelper(pi_camera, AELoggerStyle.STD_ERR)
        pi_camera_setting = PiCameraSetting(resolution=(1920, 1080), ae_loop_timeout=1)
        ending_shutter_speed = pi_camera_helper.detect_ae_shutter_speed(pi_camera_setting)

        assert (self._OPTIMAL_BRIGHTNESS_VALUE - 1) <= conversion(ending_shutter_speed) \
            <= (self._OPTIMAL_BRIGHTNESS_VALUE + 1)
