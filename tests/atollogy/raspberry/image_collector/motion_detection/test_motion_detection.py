from unittest.mock import Mock, patch
from typing import Optional
import os

from atollogy.raspberry.image_collector.motion_detection.motion_detection import MotionDetection, MotionSettings
from atollogy.raspberry.image_collector.auto_exposure.auto_exposure_setting_reader import RegionDetails
from atollogy.raspberry.image_collector.auto_exposure.ae_detection import PiCameraSetting, PiCameraHelper
from atollogy.raspberry.image_collector.auto_exposure.ae_logger import AELoggerStyle
from atollogy.raspberry.image_collector.auto_exposure.auto_exposure_setting_reader import AutoExposureSettingReader, AutoExposureSetting

import numpy as np          # pyre-ignore[21]
# Undefined import [21]: Could not find a module corresponding to import


class PiCameraHelperForTesting(PiCameraHelper):
    def __init__(self, camera) -> None:
        PiCameraHelper.__init__(self, camera, AELoggerStyle.STD_ERR)

    def detect_ae_shutter_speed(self, pi_camera_setting, region_obj = None) -> int:
        return 5000

    def take_rgb_stream_np_array(self) -> np.ndarray:        # pyre-ignore[11]
        #  Undefined type [11]: Type `np.ndarray` is not defined.
        return np.random.rand(*self._camera.resolution, 3)


class TestMotionDetection:
    def test_motion_detection_setup_scan_motion(self):
        pi_camera = Mock()
        pi_camera_setting = PiCameraSetting(resolution=(1920, 1080))
        pi_camera_helper = PiCameraHelperForTesting(pi_camera)
        motion_settings = MotionSettings()

        motion_images_storage_dir_name = os.path.join(os.getcwd(), "tests_data/motion_out")
        auto_exposure_storage_file_path = os.path.join(os.getcwd(), "tests_data/setting_cache")

        with patch('os.path.isdir'), patch('atollogy.raspberry.image_collector.motion_detection.motion_detection.MotionDetection._delete_tmp_jpeg_files'):
            detect_motion = MotionDetection(
                pi_camera,
                pi_camera_setting,
                pi_camera_helper,
                motion_settings,
                motion_images_storage_dir_name,
                auto_exposure_storage_file_path
            )

            assert detect_motion._scan_motion() == True

        # This is necessary to prevent pytest from hanging on exit
        detect_motion._state_machine.shutdown()

    def test_motion_detection_capture_sequencen(self):
        pi_camera = Mock()
        pi_camera_setting = PiCameraSetting(resolution=(1920, 1080))
        pi_camera_helper = PiCameraHelperForTesting(pi_camera)
        motion_settings = MotionSettings()

        motion_images_storage_dir_name = os.path.join(os.getcwd(), "tests_data")
        auto_exposure_storage_file_path = os.path.join(os.getcwd(), "tests_data")

        with patch('os.path.isdir'), patch('atollogy.raspberry.image_collector.motion_detection.motion_detection.MotionDetection._delete_tmp_jpeg_files'):
            detect_motion = MotionDetection(
                pi_camera,
                pi_camera_setting,
                pi_camera_helper,
                motion_settings,
                motion_images_storage_dir_name,
                auto_exposure_storage_file_path
            )

            with patch('os.rename'), patch('os.listdir', lambda x: []):
                detect_motion.capture_sequence()

        # ensure we capture the correct number of files
        assert len(pi_camera.mock_calls) == motion_settings.capture_count

        # This is necessary to prevent pytest from hanging on exit
        detect_motion._state_machine.shutdown()
