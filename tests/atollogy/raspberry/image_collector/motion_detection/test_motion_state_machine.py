import time
from atollogy.raspberry.image_collector.motion_detection.motion_state_machine \
    import MotionStateMachine, _TEST_LED_CALL_COUNTER, LEDColor

# NOTE: Minor delays are used here to mimic code processing time in introducing
# any async issues in the code. In motion_detection.py, there is no guarantee
# of the time between the states other than the time taken to execute the code.

class TestMotionStateMachine:
    def test_full_state_cycle(self):
        """Tests a normal cycle from motion_detection.py"""
        machine = MotionStateMachine()
        machine.start()
        time.sleep(0.1)
        assert machine.get_cur_state() == "startup"

        machine.safe_transition_to_ready()
        time.sleep(0.1)
        assert machine.get_cur_state() == "ready"

        machine.safe_transition_to_ae()
        time.sleep(0.1)
        assert machine.get_cur_state() == "wait_on_ae"

        machine.safe_transition_to_capture()
        time.sleep(0.1)
        assert machine.get_cur_state() == "wait_on_capture"

        machine.safe_transition_to_notify()
        time.sleep(0.1)
        assert machine.get_cur_state() == "notify"

        machine.safe_transition_to_ready()
        time.sleep(0.1)
        assert machine.get_cur_state() == "ready"

        machine.shutdown()

    def test_state_cycle_no_capture(self):
        """Tests repeated oscillations between AE and
        ready created by the motion_detection loop"""
        machine = MotionStateMachine()
        machine.start()
        time.sleep(0.1)

        for i in range(10):
            machine.safe_transition_to_ready()
            time.sleep(0.1)
            machine.safe_transition_to_ae()
            time.sleep(0.1)

        assert _TEST_LED_CALL_COUNTER._led_call_stat[LEDColor.GREEN] >= 20
        assert _TEST_LED_CALL_COUNTER._led_call_stat[LEDColor.YELLOW] == 0
        assert _TEST_LED_CALL_COUNTER._led_call_stat[LEDColor.BLUE] == 0
        assert _TEST_LED_CALL_COUNTER._led_call_stat[LEDColor.NONE] >= 1

        machine.shutdown()
