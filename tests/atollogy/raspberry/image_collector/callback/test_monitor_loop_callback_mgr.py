import random
from time import sleep
from typing import Iterator, Dict

from atollogy.raspberry.image_collector.callback.monitor_loop_callback_mgr import MonitorLoopCallbackMgr

foo_count = 0

def foo_callback_01(counter,random_value):
    print("foo_callback_01: counter={} random_value={}".format(counter,random_value))
    # using sleep to emulate network IO
    print("foo_callback_01 - sleep begin")
    sleep(1.5)
    global foo_count
    foo_count = foo_count + 1
    print("foo_callback_01 - sleep end")

class Bar:

    bar_count = 0

    def bar_callback_02(self,counter,random_value):
        print("bar_callback_02: counter={} random_value={}".format(counter, random_value))
        print("bar_callback_02 - sleep begin")
        sleep(1.5)
        self.bar_count = self.bar_count + 1
        print("bar_callback_02 - sleep end")

class FooBarMonitor(MonitorLoopCallbackMgr):
    _counter = 0

    def monitor_callee_iterator(self) -> Iterator[Dict]:
        while True:
            for i in range(0,5):
                print("monitor inner sleep: {} -- BEGIN".format(i))
                sleep(1)
                print("monitor inner sleep: {} -- END".format(i))
                if self.get_state() == 3:
                    print("monitor is stopped (inner loop)")
                    return
            self._counter = self._counter + 1
            yield { "counter" : self._counter, "random_value": random.randint(1, 10) }
            if self.get_state() == 3:
                print("monitor is stopped (after yield)")
                return


class TestMonitorLoopCallbackMgr:

    def test_manual_start_shutdown(self):
        global foo_count
        foo_count = 0
        bar_obj = Bar()

        monitor = FooBarMonitor(callback_list=[foo_callback_01, bar_obj.bar_callback_02],
                                thread_pool_size=3)
        monitor.start()
        sleep(12)
        print(" ===========> about to shutdown ")
        monitor.shutdown()
        print(" ===========> shutdown is invoked ")

        sleep(3)

        assert (2==bar_obj.bar_count)
        assert (2==foo_count)

    def test_with_statement(self):
        global foo_count
        foo_count = 0
        bar_obj = Bar()
        with FooBarMonitor(callback_list=[foo_callback_01, bar_obj.bar_callback_02],
                           thread_pool_size=3) as monitor:
            sleep(12)
            # testing auto-closing

        sleep(3)

        assert (2==bar_obj.bar_count)
        assert (2==foo_count)