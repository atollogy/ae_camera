#! /usr/bin/python3.6

# pyre-strict

from argparse import ArgumentParser
import json
import logging
import logging.handlers
import os
import os.path
import psutil # pyre-ignore[21]
import picamera # pyre-ignore[21]
import picamera.array # pyre-ignore[21]
from typing import Dict, Any, cast
import numpy as np # pyre-ignore[21]

from atollogy.raspberry.image_collector.auto_exposure.ae_detection \
    import PiCameraHelper, PiCameraSetting
from atollogy.raspberry.image_collector.auto_exposure.ae_logger import AELoggerStyle

from atollogy.raspberry.image_collector.auto_exposure.auto_exposure_setting_reader \
    import RegionDetails

from atollogy.raspberry.image_collector.motion_detection.motion_detection \
    import MotionDetection, MotionSettings
# Undefined import [21]: Could not find a module corresponding to import


LOGGER = logging.getLogger("motion_detection") #pyre-ignore[5]

LOGGER.setLevel(logging.DEBUG)

# pyre-ignore[5]
_BASE_DIR = "/var/lib/atollogy/ae_camera"
# pyre-ignore[5]
PROCESS_ID_STORAGE_FILE_PATH = _BASE_DIR + "/ae_motion_pid.json"
# pyre-ignore[5]
MOTION_IMAGES_STORAGE_DIR_NAME = _BASE_DIR + "/motion_out"
# pyre-ignore[5]
AUTO_EXPOSURE_STORAGE_FILE_PATH = _BASE_DIR + "/setting_cache"

# Missing attribute annotation [5]:
# Globally accessible variable `...` of class `...` has type `...` but no type is specified.


def main() -> None:
    parser = ArgumentParser(description='Motion Configuration parameters parser')

    '''Motion Config. parameters'''
    parser.add_argument('--captureGap', type=int, dest='capture_gap')
    parser.add_argument('--captureCount', type=int, dest='capture_count')
    parser.add_argument('--threshold', type=int, dest='threshold')
    parser.add_argument('--heartbeat', action='store_true', dest='heartbeat')

    '''Auto Exposure Loop Parameters'''
    parser.add_argument('--optimalBrightness', type=int, dest='optimal_brightness')
    parser.add_argument('--regionname', type=str, dest='region_name')
    parser.add_argument('--region', nargs=4, type=int, dest='region')

    '''Camera initialization parameters'''
    parser.add_argument('--resolution', nargs=2, type=int, dest='resolution')
    parser.add_argument('--iso', choices=[0, 100, 200, 400, 800], type=int, dest='iso')
    parser.add_argument('--maxframerate', '-f', type=float, dest='max_framerate')
    parser.add_argument('--rotation', type=int, dest='rotation')

    args = parser.parse_args()
    args = vars(args)
    args = {k:v for k, v in args.items() if v is not None}

    os.makedirs(MOTION_IMAGES_STORAGE_DIR_NAME, exist_ok=True)
    os.makedirs(AUTO_EXPOSURE_STORAGE_FILE_PATH, exist_ok=True)

    check_pid_file()

    # Motion settings
    capture_count = args["capture_count"] if "capture_count" in args else 3
    capture_gap = args["capture_gap"] if "capture_gap" in args else 1
    threshold = args["threshold"] if "threshold" in args else 28000
    heartbeat = args["heartbeat"] if "heartbeat" in args else False

    # PiCamera settings
    iso = args["iso"] if "iso" in args else 800
    max_framerate = args["max_framerate"] if "max_framerate" in args else 20
    resolution = args["resolution"] if "resolution" in args else (1920, 1080)
    rotation = args["rotation"] if "rotation" in args else 0

    region = args["region"] if "region" in args else None
    region_name = args["region_name"] if "region_name" in args else None
    optimal_brightness_value = args["optimal_brightness"] if "optimal_brightness" in args else None

    motion_settings = MotionSettings(capture_count=capture_count, capture_gap=capture_gap,
                                     threshold=threshold, heartbeat=heartbeat)

    region_details = None
    if region and region_name:
        region_details = RegionDetails(region_name=region_name,
                                   min_x=region[0], min_y=region[2],
                                   width=region[1]-region[0],
                                   height=region[3]-region[2],
                                   optimal_brightness_value=optimal_brightness_value)
    else:
        region_details = RegionDetails(region_name="__default_region__",
            optimal_brightness_value=optimal_brightness_value)

    with picamera.PiCamera() as camera:

        def take_rgb_stream_np_array()-> np.ndarray:        # pyre-ignore[11]
            # Undefined type [11]: Type `np.ndarray` is not defined.

            with picamera.array.PiRGBArray(camera) as stream:
                camera.capture(stream, format='rgb')
                return stream.array

        pi_camera_setting = PiCameraSetting(resolution=resolution, iso=iso,
                                            max_framerate=max_framerate, rotation=rotation)
        pi_camera_helper = PiCameraHelper(camera, AELoggerStyle.LOGGER)
        pi_camera_helper.take_rgb_stream_np_array = take_rgb_stream_np_array
        pi_camera_helper.init_camera_setting(pi_camera_setting)

        detect_motion = MotionDetection(camera, pi_camera_setting, pi_camera_helper,
                                        motion_settings, MOTION_IMAGES_STORAGE_DIR_NAME,
                                        AUTO_EXPOSURE_STORAGE_FILE_PATH,
                                        region_details = region_details)
        detect_motion.start_motion_detection()
        LOGGER.info("Started Motion Loop")


def check_pid_file() -> None:
    if os.path.exists(PROCESS_ID_STORAGE_FILE_PATH):
        LOGGER.info("PID file = {}; File Existence = {}".format(
                    PROCESS_ID_STORAGE_FILE_PATH,
                    os.path.exists(PROCESS_ID_STORAGE_FILE_PATH)))
        with open(PROCESS_ID_STORAGE_FILE_PATH) as pid_file:
            try:
                result = json.load(pid_file)
                result = cast(Dict[str, Any], result)
                process_id = result["ae_motion_pid"]
                LOGGER.info("PID from existing file = {}".format(process_id))
                if psutil.pid_exists(process_id):
                    exit(1)
                else:
                    data = {}
                    data["ae_motion_pid"] = os.getpid()
                    LOGGER.info("New PID = {}".format(data["ae_motion_pid"]))
                    with open(PROCESS_ID_STORAGE_FILE_PATH, 'w') as json_file:
                        json.dump(data, json_file)
            except IOError as err:
                LOGGER.error("Error in parsing JSON file. Invalid JSON format: %s ", err)
    else:
        data = {}
        data["ae_motion_pid"] = os.getpid()
        with open(PROCESS_ID_STORAGE_FILE_PATH, 'w') as json_file:
            json.dump(data, json_file)

if __name__ == "__main__":
    main()
