from setuptools import setup     # pyre-ignore[21]
# Undefined import [21]: Could not find a module corresponding to import

setup(
    name='atollogy.raspberry.image_collector',
    version='1.0.0',
    description='A Python module that handles picamera logic, motion detection logic,  and contains the new pure-Python Image Collector service',
    packages=[
        'atollogy.raspberry.image_collector.auto_exposure',
        'atollogy.raspberry.image_collector.motion_detection',
        'atollogy.raspberry.image_collector.callback',
        'atollogy.common.state_machine',
    ],
    install_requires=[
        'numpy>=1.15.0',
        'psutil>=5.4.7'
        # 'picamera==1.13'  # picamera module will be installed out-of-band
    ],
)

# Note: dependency_links has been deprecated in "pip install"
