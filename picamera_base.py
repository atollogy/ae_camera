#! /usr/bin/python3.6

# pyre-strict

import time
from argparse import ArgumentParser
from io import BytesIO
import os
import sys
from typing import Tuple, Optional

from picamera import PiCamera   # pyre-ignore[21]


class BasePiCameraObject:
    def __init__(self, camera: PiCamera, brightness: int = 50,      # pyre-ignore[11]
                 framerate: int = 30, resolution: Tuple[int, int] = (1920, 1080),
                 rotation: int = 0, exposure_mode: str = 'auto',
                 shutter_speed: int = 0, iso: int = 0, duration: int = 15) -> None:
        # Undefined type [11]: Type `PiCamera` is not defined.

        self._camera = camera
        self._camera.brightness: int = brightness
        self._camera.framerate: int = framerate
        self._camera.resolution: Tuple[int, int] = resolution
        self._camera.rotation: int = rotation
        time.sleep(1)
        self._camera.shutter_speed: int = shutter_speed
        self._camera.exposure_mode: str = exposure_mode
        self._camera.iso: int = iso
        self.duration: int = duration

    def take_picture(self) -> Optional[BytesIO]:
        try:
            image = BytesIO()
            # Camera warm-up time
            time.sleep(2)
            self._camera.capture(image, 'jpeg')
            return image
        except Exception as err:
            sys.stderr.write('Error capturing image : {}'.format(err))

    def record_video(self)-> Optional[BytesIO]:
        try:
            video = BytesIO()
            self._camera.start_recording('video.h264')
            self._camera.wait_recording(self.duration)
            self._camera.stop_recording()
            return video

        except Exception as err:
            sys.stderr.write('Error capturing video : {}'.format(err))

def main() -> None:

    parser = ArgumentParser(description='Parameters for Picamera object for taking picture')

    # Parser initialization arguments

    parser.add_argument('--brightness', type=int, dest='brightness')
    parser.add_argument('--framerate', type=float, dest='framerate')
    parser.add_argument('--resolution', nargs=2, type=int, dest='resolution')
    parser.add_argument('--rotation', type=int, dest='rotation')
    parser.add_argument('--exposure_mode', type=str, dest='exposure_mode')
    parser.add_argument('--shutter_speed', type=int, dest='shutter_speed')
    parser.add_argument('--iso', choices=[0, 100, 200, 400, 800], type=int, dest='iso')
    parser.add_argument('--capture_mode', type=str, dest='capture_mode')
    parser.add_argument('--duration', type=str, dest='duration')

    args = parser.parse_args()

    args = vars(args)
    args = {k: v for k, v in args.items() if v is not None}

    # Setting default values for PiCamera parameters

    brightness: int = args["brightness"] if "brightness" in args else 50
    framerate: int = args["framerate"] if "framerate" in args else 30.0
    resolution: Tuple[int, int] = args["resolution"] if "resolution" in args else (1920, 1080)
    rotation: int = args["rotation"] if "rotation" in args else 0
    exposure_mode: str = args["exposure_mode"] if "exposure_mode" in args else 'auto'
    shutter_speed: int = args["shutter_speed"] if "shutter_speed" in args else 0
    iso: int = args["iso"] if "iso" in args else 0
    capture_mode: str = args["capture_mode"] if "capture_mode" in args else "image"
    duration: int = args["duration"] if "duration" in args else 15

    with PiCamera() as camera:

        try:
            base_picamera_object: BasePiCameraObject = \
                BasePiCameraObject(camera, brightness, framerate,
                                   resolution, rotation, exposure_mode,
                                   shutter_speed, iso, duration)

            # Incompatible parameter type
            # Expected PiCamera object and got unknown
            # ---- Adding pyre_ignore temporarily. Need to figure out the soln and remove it soon.

            if capture_mode == "video":
                data_bytes = base_picamera_object.record_video()
            else:
                data_bytes = base_picamera_object.take_picture()

            data_bytes.seek(0) # pyre-ignore[16]
            os.write(1, data_bytes.read()) # pyre-ignore[16]

            # Undefined attribute [16]: Optional type has no attribute `seek`.
            # ---- Adding pyre_ignore temporarily. Need to figure out the soln and remove it soon.


        except Exception as err:
            sys.stderr.write('picamera_base.py: Exception: {}\n'.format(err))
        finally:
            camera.close()


if __name__ == '__main__':
    main()
